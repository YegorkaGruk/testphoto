package com.example.yegor.testphoto.activities.preference_holders;

public class PlaceholderPreference extends BasePreference {

    public static final String REMEMBER = "remember";
    public static final String TYPE = "customer";

    public static boolean isRemembered() {
        return getBool(REMEMBER, false);
    }

    public static void setRemember(boolean remember) {
        put(REMEMBER, remember);
    }

    public static int getType() {
        return getInt(TYPE, -1);
    }

    public static void setType(int type) {
        put(TYPE, type);
    }
}
