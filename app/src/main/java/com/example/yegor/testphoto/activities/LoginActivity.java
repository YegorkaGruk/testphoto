package com.example.yegor.testphoto.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.utils.Utils;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.widget.Button;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout container;
    private RelativeLayout noConnectionLayout;
    private MaterialEditText editTextLogin, editTextPassword;
    private Button btnLogin, btnCreate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        container = (LinearLayout) findViewById(R.id.container);
        noConnectionLayout = (RelativeLayout) findViewById(R.id.no_connection_layout);

        editTextLogin = (MaterialEditText) findViewById(R.id.edit_email);
        editTextPassword = (MaterialEditText) findViewById(R.id.edit_password);

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnCreate = (Button) findViewById(R.id.btn_signup);

        btnLogin.setOnClickListener(this);
        btnCreate.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        chooseLayout(Utils.hasConnection());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_login:
                if (validate())
                    onLoginSuccess();
                else
                    onLoginFail();
                break;

            case R.id.btn_signup:
                onCreateNewUser();
                break;

            case R.id.retry_btn:
                chooseLayout(Utils.hasConnection());
                break;

        }

    }

    private void chooseLayout(boolean hasConnection) {
        if (hasConnection) {
            container.setVisibility(View.VISIBLE);
            noConnectionLayout.setVisibility(View.GONE);
        } else {
            container.setVisibility(View.GONE);
            noConnectionLayout.setVisibility(View.VISIBLE);
        }
    }

    private void onCreateNewUser() {
        Intent intent = new Intent(this, NewAccountActivity.class);
        startActivity(intent);
    }

    private void onLoginFail() {
        Toast.makeText(this, "onLoginFail", Toast.LENGTH_SHORT).show();
    }

    private void onLoginSuccess() {
        Intent intent = new Intent(this, AllEventsActivity.class);
        startActivity(intent);
    }

    private boolean validate() {

        boolean valid = true;
        String login = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();

        if (login.length() == 0) {
            valid = false;
            showError(getString(R.string.error_empty_login), editTextLogin);
        }

        if (password.length() == 0) {
            valid = false;
            showError(getString(R.string.error_empty_password), editTextPassword);
        }

        if (!valid)
            return false;

        if (!login.equals(getLogin()) || !password.equals(getPassword())) {
            valid = false;
            showError(getString(R.string.error_wrong_login_or_password), editTextLogin, editTextPassword);
        }

        return valid;
    }

    public void showError(String errorMsg, MaterialEditText... inputLayouts) {
        for (MaterialEditText errorLayout : inputLayouts)
            errorLayout.setError(errorMsg);
    }

    private String getLogin() {
        return "1";
    }

    private String getPassword() {
        return "1";
    }

}