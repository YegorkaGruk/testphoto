package com.example.yegor.testphoto.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.yegor.testphoto.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;

import java.util.Arrays;

public class NewAccountActivity extends AbstractToolbarButtonsActivity implements
        FacebookCallback<LoginResult>,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = NewAccountActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 9001;
    private static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,16}$";

    private boolean valid;

    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;

    private LoginButton facebookLoginButton;
    private SignInButton signInButton;

    private MaterialEditText edit_name, edit_email, edit_password, edit_confirm_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        initToolbar();

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.DRIVE_APPFOLDER))
                .requestServerAuthCode(getString(R.string.server_client_id), false)
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        edit_name = (MaterialEditText) findViewById(R.id.edit_name);
        edit_email = (MaterialEditText) findViewById(R.id.edit_email);
        edit_password = (MaterialEditText) findViewById(R.id.edit_password);
        edit_confirm_password = (MaterialEditText) findViewById(R.id.edit_confirm_password);

        facebookLoginButton = (LoginButton) findViewById(R.id.facebook_login);
        signInButton = (SignInButton) findViewById(R.id.google_login);

        facebookLoginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        facebookLoginButton.registerCallback(callbackManager, this);

        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
        signInButton.setOnClickListener(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else
            callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.toolbar_action_done:
                if (verify())
                    Toast.makeText(NewAccountActivity.this, "action_done", Toast.LENGTH_SHORT).show();
                break;

            case R.id.toolbar_action_cancel:
                Toast.makeText(NewAccountActivity.this, "action_cancel", Toast.LENGTH_SHORT).show();
                break;

            case R.id.google_login:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;

        }

    }

    @Override
    public void onSuccess(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(

                loginResult.getAccessToken(),
                (object, response) -> {

                    String name, email;

                    try {
                        name = object.getString("name");
                        email = object.getString("email");
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        throw new RuntimeException(e.getMessage());
                    }

                    edit_name.setText(name);
                    edit_email.setText(email);
                    Log.e(TAG, object.toString());
                });

        Bundle parameters = new Bundle();

        parameters.putString("fields", "name,email");

        request.setParameters(parameters);
        request.executeAsync();

    }

    @Override
    public void onCancel() {
        Toast.makeText(NewAccountActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onError(FacebookException error) {
        Toast.makeText(NewAccountActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess() && result.getSignInAccount() != null) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String name = acct.getDisplayName();
            String email = acct.getEmail();
            String personId = acct.getId();
            Uri personPhoto = acct.getPhotoUrl();
            String idToken = acct.getIdToken();
            String authCode = acct.getServerAuthCode();

            edit_name.setText(name);
            edit_email.setText(email);

        } else {
            // Signed out, show unauthenticated UI.
        }

    }

    private boolean verify() {

        String message = "";

        String pass = edit_password.getText().toString();
        String pass_confirm = edit_confirm_password.getText().toString();

        if (!pass.matches(PASSWORD_PATTERN)) {

            if (pass.length() < 8)
                message += "too short";

            if (pass.length() > 16)
                message += "too long";

            if (!pass.matches(".*\\d+.*"))
                message += message.equals("") ? "no digit" : ", no digit";

            if (!pass.matches(".*[a-zA-Z]+.*"))
                message += message.equals("") ? "no letter" : ", no letter";

            if (!message.equals(""))
                edit_password.setError(message);

            return false;

        } else if (!pass_confirm.equals(pass)) {
            edit_confirm_password.setError("passwords doesn't match");
            return false;
        } else
            return true;

    }

}