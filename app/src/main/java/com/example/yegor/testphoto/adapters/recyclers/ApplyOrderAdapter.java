package com.example.yegor.testphoto.adapters.recyclers;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.yegor.testphoto.App;
import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.utils.ImageUtils;
import com.example.yegor.testphoto.utils.MyBlur;

import net.yazeed44.imagepicker.model.ImageEntry;
import net.yazeed44.imagepicker.util.Picker;

import java.util.ArrayList;

public class ApplyOrderAdapter extends RecyclerView.Adapter<ApplyOrderAdapter.ViewHolder2> {

    private ArrayList<ImageEntry> images;
    private Context context;

    public ApplyOrderAdapter(Context context) {
        images = new ArrayList<>(0);
        this.context = context;
    }

    public ApplyOrderAdapter(Context context, ArrayList<ImageEntry> images) {
        this.images = images;
        this.context = context;
    }

    @Override
    public ViewHolder2 onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder2(viewGroup);
    }

    @Override
    public void onBindViewHolder(final ViewHolder2 holder, int position) {
        /*
        if (images.size() == 0) {
            holder.thumbnail.setImageResource(R.drawable.add_circular_button);
            holder.onClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pickImages();
                }
            });
            return;
        }

        if (position == images.size()) {
            holder.thumbnail.setImageResource(R.drawable.add_circular_button);
            holder.onClick(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pickImages();
                }
            });
        } else {
        */
        final String path = images.get(position).path;
        holder.loadImage(path, holder.thumbnail);
        holder.textView.setText("position № " + position);
        holder.imageButton.setOnClickListener(v -> {
            images.remove(holder.getLayoutPosition());
            notifyItemRemoved(holder.getLayoutPosition());
            notifyItemRangeChanged(holder.getLayoutPosition(), images.size());
        });
        holder.cv.setOnLongClickListener(v -> {
            holder.imageButton.setVisibility(View.VISIBLE);
            holder.loadImageWithTransformation(path, holder.thumbnail);
            return true;
        });
        //}

    }

    @Override
    public int getItemCount() {
        //return images.size() + 1;
        return images.size();
    }

    private void pickImages() {
        //You can change many settings in builder like limit , Pick mode and colors
        new Picker.Builder(context, new MyPickListener(), R.style.MIP_theme)
                .build()
                .startActivity();
    }

    class ViewHolder1 extends RecyclerView.ViewHolder {

        public static final int LAYOUT_ID = R.layout.item_apply_order_header;

        private TextView additional;
        private TextView categories;
        private TextView place;
        private ImageButton showPlace;
        private TextView density;
        private TextView digitalProcessing;

        public ViewHolder1(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(LAYOUT_ID, parent, false));

            additional = (TextView) itemView.findViewById(R.id.additional);
            categories = (TextView) itemView.findViewById(R.id.categories);
            place = (TextView) itemView.findViewById(R.id.place);
            showPlace = (ImageButton) itemView.findViewById(R.id.show_place);
            density = (TextView) itemView.findViewById(R.id.density);
            digitalProcessing = (TextView) itemView.findViewById(R.id.digital_processing);

        }

        public void setAdditional(String additional) {
            this.additional.setText(additional);
        }

        public void setCategories(String[] categories) {
            this.categories.setText(TextUtils.join(", ", categories));
        }

        public void setPlace(String place) {
            this.place.setText(place);
        }

        public void setShowPlace(ImageButton showPlace) {
            throw new UnsupportedOperationException();
        }

        public void setDensity(int width, int height) {
            this.density.setText(App.getContext().getString(R.string.density, width, height));
        }

        public void setDigitalProcessing(boolean digitalProcessing) {
            this.digitalProcessing.setText(digitalProcessing ? "digitalProcessing true" : "digitalProcessing false");
        }
    }

    class ViewHolder2 extends RecyclerView.ViewHolder {

        public static final int LAYOUT_ID = R.layout.item_add_images;

        protected CardView cv;
        protected ImageView thumbnail;
        protected TextView textView;
        protected ImageButton imageButton;

        private int cvWidth;
        private int cvHeight;

        public ViewHolder2(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(LAYOUT_ID, parent, false));

            cv = (CardView) itemView.findViewById(R.id.cv);
            thumbnail = (ImageView) itemView.findViewById(R.id.image_view);
            textView = (TextView) itemView.findViewById(R.id.country_name);
            imageButton = (ImageButton) itemView.findViewById(R.id.imageButton);

            cvWidth = parent.getWidth() / 2;
        }

        public void onClick(View.OnClickListener listener) {
            thumbnail.setOnClickListener(listener);
        }

        private Drawable getSelectedItemDrawable() {
            int[] attrs = new int[]{R.attr.selectableItemBackground};
            TypedArray ta = context.obtainStyledAttributes(attrs);
            Drawable selectedItemDrawable = ta.getDrawable(0);
            ta.recycle();
            return selectedItemDrawable;
        }

        private void loadImage(final String path, final ImageView imageView) {

            cvHeight = (int) (cvWidth / ImageUtils.getImageRatio(path));

            cv.setLayoutParams(new CardView.LayoutParams(cvWidth, cvHeight));

            Glide.with(context)
                    .load(path)
                    .fitCenter()
                    .thumbnail(0.1f)
                    //.dontAnimate()
                    .into(imageView);

        }

        private void loadImageWithTransformation(final String path, final ImageView imageView) {
            //imageView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 440));
        /*
        Glide.with(context)
                .load(path)
                .placeholder(imageView.getDrawable())
                .bitmapTransform(new BlurTransformation(context))
                .into(imageView);
        */
            imageView.buildDrawingCache();
            Log.w("getDrawingCache == null", "" + (imageView.getDrawingCache() == null));
            imageView.setImageBitmap(MyBlur.doBlur(imageView.getDrawingCache()));

        }

    }

    private class MyPickListener implements Picker.PickListener {

        @Override
        public void onPickedSuccessfully(final ArrayList<ImageEntry> images) {
            ApplyOrderAdapter.this.images = images;
            notifyDataSetChanged();
        }

        @Override
        public void onCancel() {
            Toast.makeText(context, "onCancel()", Toast.LENGTH_SHORT).show();
        }

    }

}