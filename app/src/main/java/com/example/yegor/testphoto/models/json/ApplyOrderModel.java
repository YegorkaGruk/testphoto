package com.example.yegor.testphoto.models.json;

import java.util.List;

public class ApplyOrderModel {

    public int orderId;

    public int quantity;
    public int minHeight;
    public int minWidth;
    public boolean digitalProcessing;
    public String additional;

    public long postTime;
    public long deadline;

    public _MoneyModel price;
    public _PlaceModel place;

    public List<ApplyOrderExecutorModel> executors;

    public ApplyOrderModel(int orderId, int quantity, int minHeight, int minWidth,
                           boolean digitalProcessing, String additional, long postTime,
                           long deadline, _MoneyModel price, _PlaceModel place,
                           List<ApplyOrderExecutorModel> executors) {

        this.orderId = orderId;
        this.quantity = quantity;
        this.minHeight = minHeight;
        this.minWidth = minWidth;
        this.digitalProcessing = digitalProcessing;
        this.additional = additional;
        this.postTime = postTime;
        this.deadline = deadline;
        this.price = price;
        this.place = place;
        this.executors = executors;

    }

    @Override
    public String toString() {
        return "ApplyOrderModel{" +
                "orderId=" + orderId +
                ", quantity=" + quantity +
                ", minHeight=" + minHeight +
                ", minWidth=" + minWidth +
                ", digitalProcessing=" + digitalProcessing +
                ", additional='" + additional + '\'' +
                ", postTime=" + postTime +
                ", deadline=" + deadline +
                ", price=" + price +
                ", place=" + place +
                ", executors=" + executors +
                '}';
    }

    public static class Builder {

        private int orderId;
        private int quantity;
        private int minHeight;
        private int minWidth;
        private boolean digitalProcessing;
        private String additional;
        private long postTime;
        private long deadline;
        private _MoneyModel price;
        private _PlaceModel place;
        private List<ApplyOrderExecutorModel> executors;

        public Builder setOrderId(int orderId) {
            this.orderId = orderId;
            return this;
        }

        public Builder setQuantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder setMinHeight(int minHeight) {
            this.minHeight = minHeight;
            return this;
        }

        public Builder setMinWidth(int minWidth) {
            this.minWidth = minWidth;
            return this;
        }

        public Builder setDigitalProcessing(boolean digitalProcessing) {
            this.digitalProcessing = digitalProcessing;
            return this;
        }

        public Builder setAdditional(String additional) {
            this.additional = additional;
            return this;
        }

        public Builder setPostTime(long postTime) {
            this.postTime = postTime;
            return this;
        }

        public Builder setDeadline(long deadline) {
            this.deadline = deadline;
            return this;
        }

        public Builder setPrice(_MoneyModel price) {
            this.price = price;
            return this;
        }

        public Builder setPlace(_PlaceModel place) {
            this.place = place;
            return this;
        }

        public Builder setExecutors(List<ApplyOrderExecutorModel> executors) {
            this.executors = executors;
            return this;
        }

        public ApplyOrderModel create() {
            return new ApplyOrderModel(orderId, quantity, minHeight, minWidth, digitalProcessing, additional, postTime, deadline, price, place, executors);
        }

    }

}