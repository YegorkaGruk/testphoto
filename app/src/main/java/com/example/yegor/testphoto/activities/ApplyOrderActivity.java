package com.example.yegor.testphoto.activities;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.adapters.recyclers.ApplyOrderAdapter;
import com.example.yegor.testphoto.utils.ZipUtil;

import net.yazeed44.imagepicker.model.ImageEntry;
import net.yazeed44.imagepicker.util.Picker;

import java.io.IOException;
import java.util.ArrayList;

public class ApplyOrderActivity extends AbstractToolbarButtonsActivity {

    String[] chosenFiles;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_order);

        initToolbar();

        recyclerView = (RecyclerView) findViewById(R.id.rv);
        //recyclerView.addItemDecoration(new TestMarginDecoration(this));
        recyclerView.setHasFixedSize(true);

        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        //GridLayoutManager manager = new GridLayoutManager(this, 2);

        ApplyOrderAdapter adapter = new ApplyOrderAdapter(this);
        recyclerView.setAdapter(adapter);

        pickImages();


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbar_action_done:
                try {
                    Log.e("zipped", "started");
                    //TODO getFilesDir().getAbsolutePath()
                    ZipUtil.zip(chosenFiles, Environment.getExternalStorageDirectory().toString() + "/ZIP_ZIP.zip");
                } catch (IOException e) {
                    Log.e("IOException", e.getMessage());
                    throw new RuntimeException();
                } finally {
                    Log.e("zipped", "zipped");
                }
                Toast.makeText(this, "toolbar_action_done", Toast.LENGTH_SHORT).show();

                break;

            case R.id.toolbar_action_cancel:
                Toast.makeText(this,
                        "deleted - " + ZipUtil.delete(Environment.getExternalStorageDirectory().toString() + "/ZIP_ZIP.zip")
                        , Toast.LENGTH_SHORT).show();
                break;
        }

    }

    private void pickImages() {
        //You can change many settings in builder like limit , Pick mode and colors
        new Picker.Builder(this, new MyPickListener(), R.style.MIP_theme)
                .build()
                .startActivity();
    }

    private class MyPickListener implements Picker.PickListener {

        @Override
        public void onPickedSuccessfully(final ArrayList<ImageEntry> images) {
            recyclerView.setAdapter(new ApplyOrderAdapter(ApplyOrderActivity.this, images));

            chosenFiles = new String[images.size()];
            for (int i = 0; i < images.size(); i++) {
                chosenFiles[i] = images.get(i).path;
            }


        }

        @Override
        public void onCancel() {
            Toast.makeText(getBaseContext(), "onCancel()", Toast.LENGTH_SHORT).show();
        }

    }

}
