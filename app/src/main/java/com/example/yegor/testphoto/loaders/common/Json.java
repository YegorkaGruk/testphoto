package com.example.yegor.testphoto.loaders.common;

import com.example.yegor.testphoto.models.json.AllOrdersModel;
import com.example.yegor.testphoto.models.json.ApplyOrderModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Json {
    @GET("/IamEgor/TestRest/master/json/all_orders.json")
    Call<List<AllOrdersModel>> getAllOrders();

    @GET("/IamEgor/TestRest/master/json/customer_order.json")
    Call<ApplyOrderModel> getOrder();
}