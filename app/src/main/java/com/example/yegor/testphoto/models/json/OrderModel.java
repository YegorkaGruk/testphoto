package com.example.yegor.testphoto.models.json;

import java.util.List;

public class OrderModel {

    public long OrderId;

    public List<String> categories;
    public int quantity;
    public int minWidth;
    public int minHeight;
    public boolean digitalProcessing;
    public String additional;

    public long postTime;
    public long deadline;

    public _MoneyModel price;
    public _PlaceModel place;

    public OrderModel(long OrderId, List<String> categories, int quantity, int minWidth, int minHeight,
                      boolean digitalProcessing, long postTime, long deadline, String additional,
                      _MoneyModel price, _PlaceModel place) {

        this.OrderId = OrderId;
        this.categories = categories;
        this.quantity = quantity;
        this.minWidth = minWidth;
        this.minHeight = minHeight;
        this.digitalProcessing = digitalProcessing;
        this.postTime = postTime;
        this.deadline = deadline;
        this.additional = additional;
        this.price = price;
        this.place = place;

    }

    public static class Builder {

        private long OrderId;
        private List<String> categories;
        private int quantity;
        private int minWidth;
        private int minHeight;
        private boolean digitalProcessing;
        private long postTime;
        private long deadline;
        private String additional;
        private _MoneyModel price;
        private _PlaceModel place;

        public Builder setOrderId(long orderId) {
            this.OrderId = orderId;
            return this;
        }

        public Builder setCategories(List<String> categories) {
            this.categories = categories;
            return this;
        }

        public Builder setQuantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder setMinWidth(int minWidth) {
            this.minWidth = minWidth;
            return this;
        }

        public Builder setMinHeight(int minHeight) {
            this.minHeight = minHeight;
            return this;
        }

        public Builder setDigitalProcessing(boolean digitalProcessing) {
            this.digitalProcessing = digitalProcessing;
            return this;
        }

        public Builder setPostTime(long postTime) {
            this.postTime = postTime;
            return this;
        }

        public Builder setDeadline(long deadline) {
            this.deadline = deadline;
            return this;
        }

        public Builder setAdditional(String additional) {
            this.additional = additional;
            return this;
        }

        public Builder setPrice(_MoneyModel price) {
            this.price = price;
            return this;
        }

        public Builder setPlace(_PlaceModel place) {
            this.place = place;
            return this;
        }

        public OrderModel create() {
            return new OrderModel(OrderId, categories, quantity, minWidth, minHeight,
                    digitalProcessing, postTime, deadline, additional, price, place);
        }

    }

}
