package com.example.yegor.testphoto.utils;

import com.alexvasilkov.gestures.views.interfaces.GestureView;

public interface GestureSettingsSetupListener {
    void onSetupGestureView(GestureView view);
}