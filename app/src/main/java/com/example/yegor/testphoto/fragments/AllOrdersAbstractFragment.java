package com.example.yegor.testphoto.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.yegor.testphoto.App;
import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.loaders.common.ContentWrapper;
import com.example.yegor.testphoto.loaders.common.Json;
import com.example.yegor.testphoto.loaders.common.NoConnectionException;
import com.example.yegor.testphoto.loaders.support.AbstractJsonLoaderSupport;
import com.example.yegor.testphoto.models.json.AllOrdersModel;

import java.util.List;

import retrofit2.Call;

public abstract class AllOrdersAbstractFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<ContentWrapper<List<AllOrdersModel>>> {

    public static final String ACTION = App.getContext().getPackageName();

    @LayoutRes
    int contentId;
    private View loadingIndicator;
    private View noConnectionLayout;
    private View content;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getLoaderManager()
                    .restartLoader(0, null, AllOrdersAbstractFragment.this)
                    .forceLoad();
        }
    };

    public AllOrdersAbstractFragment(int content) {
        super();
        this.contentId = content;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(receiver, new IntentFilter(ACTION));
    }

    @Override
    public void onDetach() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(receiver);
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.test_abstr_layout, container, false);

        content = inflater.inflate(contentId, container, false);
        loadingIndicator = rootView.findViewById(R.id.loading_indicator);
        noConnectionLayout = rootView.findViewById(R.id.no_connection_layout);
        FrameLayout container1 = (FrameLayout) rootView.findViewById(R.id.container);

        container1.addView(content);

        rootView.findViewById(R.id.retry_btn).setOnClickListener(v ->
                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(ACTION)));

        getLoaderManager().initLoader(0, null, this).forceLoad();

        return rootView;
    }

    @Override
    public Loader<ContentWrapper<List<AllOrdersModel>>> onCreateLoader(int id, Bundle args) {

        setViewContent(Status.LOADING);

        return new AbstractJsonLoaderSupport<List<AllOrdersModel>>(getContext()) {
            @Override
            public Call<List<AllOrdersModel>> getMethod(Json service) {
                return service.getAllOrders();
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<ContentWrapper<List<AllOrdersModel>>> loader, ContentWrapper<List<AllOrdersModel>> data) {

        if (data.getException() == null && data.getContent() != null) {
            onDataReceived(data.getContent());
            setViewContent(Status.OK);
        } else if (data.getException() instanceof NoConnectionException)
            setViewContent(Status.FAILED_NO_CONNECTION);
        else
            throw new RuntimeException("Unknown exception");

    }

    protected abstract void onDataReceived(List<AllOrdersModel> models);

    private void setViewContent(Status status) {

        switch (status) {
            case LOADING:
                loadingIndicator.setVisibility(View.VISIBLE);
                content.setVisibility(View.GONE);
                noConnectionLayout.setVisibility(View.GONE);
                break;
            case OK:
                content.setVisibility(View.VISIBLE);
                loadingIndicator.setVisibility(View.GONE);
                noConnectionLayout.setVisibility(View.GONE);
                break;
            case FAILED_NO_CONNECTION:
                noConnectionLayout.setVisibility(View.VISIBLE);
                content.setVisibility(View.GONE);
                loadingIndicator.setVisibility(View.GONE);
                break;
        }

    }


    //TODO @IntDef
    enum Status {
        LOADING, OK, FAILED_NO_CONNECTION
    }

}
