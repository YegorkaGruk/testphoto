package com.example.yegor.testphoto.models.adapter;

public class Order {

    private String shortDescription;
    private String fullDescription;
    private String price;
    private String timeFormat;

    public String getShortDescription() {
        return shortDescription;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public String getPrice() {
        return price;
    }

    public String getTimeFormat() {
        return timeFormat;
    }

}
