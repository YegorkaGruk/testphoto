package com.example.yegor.testphoto.adapters.recyclers.holders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.yegor.testphoto.Extractor;
import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.models.json.ApplyOrderExecutorImageModel;
import com.example.yegor.testphoto.utils.glide.GlideHelper;
import com.rey.material.widget.CheckBox;

public class CustomerOrderImageHolder extends RecyclerView.ViewHolder implements ErasableHolder {

    public static final int LAYOUT_ID = R.layout.item_image;

    private Extractor extractor = new Extractor();

    private CardView cv;
    private ImageView image;
    private CheckBox checkBox;

    private int cvWidth;
    private int cvHeight;
    private boolean checked;

    public CustomerOrderImageHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(LAYOUT_ID, parent, false));
        initViews(parent);
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ApplyOrderExecutorImageModel model) {

        //GlideHelper.loadThumb(model, itemViewHolder.imageView);

        cvHeight = (int) (cvWidth / extractor.extractRatio(model.url));

        Log.w(" setImage ",
                " cvHeight - " + cvHeight +
                        " cvWidth " + cvWidth +
                        " ratio - " + extractor.extractRatio(model.url) +
                        " url : " + model.url);

        cv.setLayoutParams(new CardView.LayoutParams(cvWidth, cvHeight));

        GlideHelper.loadThumb(model, image);
        /*
        Glide.with(App.getContext())
                .load(model.url)
                //.thumbnail(0.1f)
                .fitCenter()
                .override(cvWidth, cvHeight)
                .crossFade(500)
                //.bitmapTransform(new BlurTransformation(App.getContext()))
                .into(image);
        */
    }

    @Override
    public void erase() {
        Log.w("erase", "ImageHolder.erase() ");
        Glide.clear(image);
        image.setImageDrawable(null);
    }

    private void initViews(ViewGroup parent) {
        cvWidth = parent.getWidth() / 2;
        initViews();
    }

    private void initViews() {

        cv = (CardView) itemView.findViewById(R.id.cv);
        image = (ImageView) itemView.findViewById(R.id.image);
        checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);

        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> checked = isChecked);
        /*
        if (!Utils.isLollipop())
            MaterialRippleLayout.on(image)
                    .rippleOverlay(true)
                    .rippleAlpha(0.5f)
                    .rippleColor(0xFFFF5858)
                    .rippleHover(true)
                    .create();
        */

    }

    public void setOnImageListener(View.OnClickListener listener) {
        image.setOnClickListener(listener);
    }

}
