package com.example.yegor.testphoto.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.example.yegor.testphoto.loaders.common.ContentWrapper;
import com.example.yegor.testphoto.loaders.common.Json;
import com.example.yegor.testphoto.loaders.common.Loader;
import com.example.yegor.testphoto.loaders.common.NoConnectionException;
import com.example.yegor.testphoto.utils.Utils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class AbstractJsonLoader<T>
        extends AsyncTaskLoader<ContentWrapper<T>>
        implements Loader<T> {

    public static final String TAG = AbstractJsonLoader.class.getName();

    private static final long DEFAULT_DELAY = 750L;
    private final String BASE_URL = "https://raw.githubusercontent.com/";

    public AbstractJsonLoader(Context context) {
        super(context);
    }

    @Override
    public ContentWrapper<T> loadInBackground() {

        if (!Utils.hasConnection()) {
            try {
                Thread.sleep(DEFAULT_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return new ContentWrapper<>(new NoConnectionException());
        }

        ContentWrapper<T> content = new ContentWrapper<>();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Json service = retrofit.create(Json.class);

        Call<T> data = getMethod(service);

        try {
            Response<T> response = data.execute();
            content.setContent(response.body());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            content.setException(e);
        }

        return content;
    }


}
