package com.example.yegor.testphoto.loaders.common;

import retrofit2.Call;

public interface Loader<T> {
    Call<T> getMethod(Json service);
}