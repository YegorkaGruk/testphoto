package com.example.yegor.testphoto.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.adapters.fragments.SectionsPagerAdapter;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.rey.material.widget.TabPageIndicator;

public class AllEventsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private FloatingActionButton fabMenuMap;
    private FloatingActionMenu fabMenuList;
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_events);

        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        NavigationView nvDrawer = (NavigationView) findViewById(R.id.nvView);

        fabMenuMap = (FloatingActionButton) findViewById(R.id.fab_menu_map);
        fabMenuList = (FloatingActionMenu) findViewById(R.id.fab_menu_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        setSupportActionBar(toolbar);
        setupDrawerContent(nvDrawer);

        drawerToggle = setupDrawerToggle();
        mDrawer.setDrawerListener(drawerToggle);

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(1);

        TabPageIndicator tabPageIndicator = (TabPageIndicator) findViewById(R.id.tabs);
        tabPageIndicator.setViewPager(mViewPager);

        createCustomAnimation(fabMenuList, R.drawable.ic_sort_24dp);

        findViewById(R.id.fab).setOnClickListener(view -> {
            Intent intent = new Intent(AllEventsActivity.this, CreateOrderActivity.class);
            startActivity(intent);
        });

        mViewPager.addOnPageChangeListener(
                new OnPageSelected() {
                    @Override
                    public void onPageSelected(int position) {

                        switch (position) {

                            case 0:

                                if (!fabMenuList.isMenuButtonHidden())
                                    fabMenuList.hideMenuButton(true);

                                fabMenuMap.postDelayed(() -> fabMenuMap.show(true), 200);

                                break;

                            case 1:

                                if (!fabMenuMap.isHidden())
                                    fabMenuMap.hide(true);

                                fabMenuList.postDelayed(() -> fabMenuList.showMenuButton(true), 200);

                                break;

                            case 2:

                                if (!fabMenuMap.isHidden())
                                    fabMenuMap.hide(true);

                                if (!fabMenuList.isMenuButtonHidden())
                                    fabMenuList.hideMenuButton(true);

                        }
                    }
                });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_all_events, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    // `onPostCreate` called when activity start-up is complete after `onStart()`
    // NOTE! Make sure to override the method with only a single `Bundle` argument
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerContent(NavigationView navigationView) {

        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    selectDrawerItem(menuItem);
                    return true;
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.profile:
                Toast.makeText(AllEventsActivity.this, "Profile", Toast.LENGTH_SHORT).show();
                break;
            case R.id.settings:
                Toast.makeText(AllEventsActivity.this, "Settings", Toast.LENGTH_SHORT).show();
                break;
            case R.id.logout:
                Toast.makeText(AllEventsActivity.this, "Logout", Toast.LENGTH_SHORT).show();
                break;

        }

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Close the navigation drawer
        mDrawer.closeDrawers();

    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    private void createCustomAnimation(final FloatingActionMenu fabMenu, @DrawableRes int pic) {

        AnimatorSet set = new AnimatorSet();

        ObjectAnimator scaleOutX = ObjectAnimator.ofFloat(fabMenu.getMenuIconView(), "scaleX", 1.0f, 0.2f);
        ObjectAnimator scaleOutY = ObjectAnimator.ofFloat(fabMenu.getMenuIconView(), "scaleY", 1.0f, 0.2f);

        ObjectAnimator scaleInX = ObjectAnimator.ofFloat(fabMenu.getMenuIconView(), "scaleX", 0.2f, 1.0f);
        ObjectAnimator scaleInY = ObjectAnimator.ofFloat(fabMenu.getMenuIconView(), "scaleY", 0.2f, 1.0f);

        scaleOutX.setDuration(50);
        scaleOutY.setDuration(50);

        scaleInX.setDuration(150);
        scaleInY.setDuration(150);

        scaleInX.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                fabMenu.getMenuIconView().setImageResource(fabMenu.isOpened()
                        ? R.drawable.ic_close_24dp : pic);
            }
        });

        set.play(scaleOutX).with(scaleOutY);
        set.play(scaleInX).with(scaleInY).after(scaleOutX);
        set.setInterpolator(new OvershootInterpolator(2));

        fabMenu.setIconToggleAnimatorSet(set);

    }

    abstract class OnPageSelected implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int state) {
            Log.w("onScrollStateChanged", " state  - " + state);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

    }

}

