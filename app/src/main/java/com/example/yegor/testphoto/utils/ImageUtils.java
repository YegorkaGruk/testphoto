package com.example.yegor.testphoto.utils;

import android.graphics.BitmapFactory;

/**
 * Created by yegor on 22.04.16.
 */
public class ImageUtils {

    public static Dimens getImageDimens(String path) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        //Returns null, sizes are in the options variable
        BitmapFactory.decodeFile(path, options);

        return new Dimens(options.outWidth, options.outHeight);
    }

    public static float getImageRatio(String path) {
        return getImageDimens(path).getRatio();
    }

    public static class Dimens {

        public int width;
        public int height;

        public Dimens(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public float getRatio() {
            return 1f * width / height;
        }

    }

}
