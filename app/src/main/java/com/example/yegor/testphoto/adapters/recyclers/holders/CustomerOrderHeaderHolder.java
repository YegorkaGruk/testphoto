package com.example.yegor.testphoto.adapters.recyclers.holders;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.example.yegor.testphoto.App;
import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.utils.Utils;
import com.github.florent37.viewanimator.ViewAnimator;

import java.util.Locale;

public class CustomerOrderHeaderHolder extends RecyclerView.ViewHolder implements ErasableHolder {

    public static final int LAYOUT_ID = R.layout.item_header;

    private CardView cv;
    private ImageButton expand;
    private ImageView avatar;
    private TextView name;
    private TextView statistics;

    private boolean isExpanded = false;

    public CustomerOrderHeaderHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(LAYOUT_ID, parent, false));

        initViews();
    }

    @Override
    public void erase() {
        //Glide.clear(avatar);
    }

    public void setAvatar(String url) {
        Glide.with(App.getContext()).load(url).priority(Priority.HIGH).fitCenter().into(avatar);
    }

    public void setAvatarText(String text) {
        TextDrawable drawable = TextDrawable.builder()
                .buildRect("A", Color.RED);
        avatar.setImageDrawable(drawable);
    }

    public void setName(String name) {
        this.name.setText(name);
    }

    private void initViews() {

        cv = (CardView) itemView.findViewById(R.id.cv);
        expand = (ImageButton) itemView.findViewById(R.id.expand);
        avatar = (ImageView) itemView.findViewById(R.id.avatar);
        name = (TextView) itemView.findViewById(R.id.name);
        statistics = (TextView) itemView.findViewById(R.id.statistic);

        if (!Utils.isLollipop())
            expand.setBackgroundDrawable(Utils.getRippleDrawable(R.drawable.ripple_background));

        expand.setOnClickListener(v -> {
            if (!isExpanded)
                expand();
            else
                collapse();
            isExpanded = !isExpanded;
        });

        setStatistics(25, 11);

        /*
        if (!Utils.isLollipop())
            MaterialRippleLayout.on(container)
                    .rippleOverlay(true)
                    .rippleAlpha(0.5f)
                    .rippleColor(0xFF585858)
                    .rippleHover(true)
                    .create();
        */
    }

    private void expand() {
        ViewAnimator
                .animate(cv)
                .dp().height(36, 132)

                .andAnimate(name)
                .dp()
                .translationX(132 - 16) //
                .translationY(0, 16)
                .duration(350)

                .andAnimate(expand)
                .rotation(180)

                //.accelerate()
                .onStart(() -> {
                    expand.setEnabled(false);

                    RelativeLayout.LayoutParams layoutParams =
                            (RelativeLayout.LayoutParams) name.getLayoutParams();
                    layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, 0);
                })
                .onStop(() -> {
                    avatar.setVisibility(View.VISIBLE);
                    avatar.setAlpha(0f);

                    name.setTranslationX(0);
                    name.setTranslationY(0);

                    statistics.setVisibility(View.VISIBLE);
                    statistics.setAlpha(0f);
                })

                .thenAnimate(avatar)
                .alpha(0f, 1f)
                .duration(350)

                .thenAnimate(statistics)
                .alpha(0f, 1f)
                .duration(350)

                .onStop(() -> expand.setEnabled(true))

                .start();

    }

    private void collapse() {
        ViewAnimator
                .animate(statistics)
                .alpha(1f, 0f)
                .duration(350)

                .andAnimate(expand)
                .rotation(360)

                .onStart(() -> expand.setEnabled(false))

                .thenAnimate(avatar)
                .alpha(1f, 0f)
                .duration(350)                                  //может и не надо

                .thenAnimate(cv)
                .dp().height(132, 36)
                .andAnimate(name)
                .dp()
                .translationX(132 - 16, 16) //
                .translationY(16, 0)
                .duration(350)
                //.accelerate()
                .onStart(() -> {
                    avatar.setVisibility(View.GONE);
                    statistics.setVisibility(View.GONE);

                    name.setTranslationX(Utils.convertDpToPixel(132f - 16f));
                    name.setTranslationY(Utils.convertDpToPixel(16f));
                })
                .onStop(() -> {
                    expand.setEnabled(true);

                    RelativeLayout.LayoutParams layoutParams =
                            (RelativeLayout.LayoutParams) name.getLayoutParams();
                    layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
                })

                .start();

    }

    public void setStatistics(int applied, int accepted) {

        String s = getStringFormat(applied, accepted);

        Spannable wordtoSpan = new SpannableString(s);
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 0, s.indexOf(" / "), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.BLACK), s.indexOf("/"), s.indexOf("/") + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.GREEN), s.indexOf("/") + 1, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        statistics.setText(wordtoSpan);
    }

    public String getStringFormat(int applied, int accepted) {
        return String.format(
                Locale.getDefault(),
                App.getContext().getString(R.string.statistics_message),
                applied, accepted);
    }


}
