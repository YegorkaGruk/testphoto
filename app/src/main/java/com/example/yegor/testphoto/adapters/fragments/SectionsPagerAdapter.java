package com.example.yegor.testphoto.adapters.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.yegor.testphoto.fragments.AllOrdersListFragment;
import com.example.yegor.testphoto.fragments.AllOrdersMapFragment;
import com.example.yegor.testphoto.fragments.PlaceholderFragment;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                //return AllOrdersMapFragment2.newInstance();
                return AllOrdersMapFragment.newInstance();
            case 1:
                return AllOrdersListFragment.newInstance();
            case 2:
                return PlaceholderFragment.newInstance(position + 1);
            default:
                return PlaceholderFragment.newInstance(0);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "On map";
            case 1:
                return "All Orders";
            case 2:
                return "Noticed";
        }
        return null;
    }
}