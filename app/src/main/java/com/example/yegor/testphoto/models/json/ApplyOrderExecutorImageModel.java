package com.example.yegor.testphoto.models.json;

public class ApplyOrderExecutorImageModel extends AbstractModel {

    public int id;
    public String url;
    public int width;
    public int height;
    public String thumbnail;
    public int dominantColor;// = 0xFF00FF;
    public int ownerId;

    public ApplyOrderExecutorImageModel(int id, String url, int width, int height, String thumbnail, int dominantColor, int ownerId) {
        this.id = id;
        this.url = url;
        this.width = width;
        this.height = height;
        this.thumbnail = thumbnail;
        this.dominantColor = dominantColor;
        this.ownerId = ownerId;
    }

    @Override
    public String toString() {
        return "ApplyOrderExecutorImageJsonModel{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", thumbnail='" + thumbnail + '\'' +
                ", dominantColor=" + dominantColor +
                ", ownerId=" + ownerId +
                '}';
    }

    public static class Builder {

        private int id;
        private String url;
        private int width;
        private int height;
        private String thumbnail;
        private int dominantColor;
        private int ownerId;

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setWidth(int width) {
            this.width = width;
            return this;
        }

        public Builder setHeight(int height) {
            this.height = height;
            return this;
        }

        public Builder setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
            return this;
        }

        public Builder setDominantColor(int dominantColor) {
            this.dominantColor = dominantColor;
            return this;
        }

        public Builder setOwnerId(int ownerId) {
            this.ownerId = ownerId;
            return this;
        }

        public ApplyOrderExecutorImageModel create() {
            return new ApplyOrderExecutorImageModel(id, url, width, height, thumbnail, dominantColor, ownerId);
        }

    }

}
