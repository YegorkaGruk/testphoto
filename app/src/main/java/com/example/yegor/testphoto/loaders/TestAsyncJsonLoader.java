package com.example.yegor.testphoto.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.example.yegor.testphoto.models.json.AllOrdersModel;
import com.example.yegor.testphoto.models.json.ApplyOrderModel;
import com.example.yegor.testphoto.utils.ServiceGenerator;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public class TestAsyncJsonLoader extends AsyncTaskLoader<List<AllOrdersModel>> {

    public static final String TAG = TestAsyncJsonLoader.class.getName();
    final String BASE_URL = "https://raw.githubusercontent.com/";

    public TestAsyncJsonLoader(Context context) {
        super(context);
    }

    @Override
    public List<AllOrdersModel> loadInBackground() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Json service = retrofit.create(Json.class);

        Call<List<AllOrdersModel>> data = service.getAllOrders();

        try {
            Response<List<AllOrdersModel>> response = data.execute();

            for (AllOrdersModel model : response.body())
                Log.e("AllOrdersJsonModels", model.toString());

            return response.body();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            throw new RuntimeException();
        }
        /*
        data.enqueue(new Callback<List<AllOrdersJsonModel>>() {
            @Override
            public void onResponse(Call<List<AllOrdersJsonModel>> call, Response<List<AllOrdersJsonModel>> response) {
                Log.e("AllOrdersJsonModel model", "onResponse");
                List<AllOrdersJsonModel> resp =  response.body();
                Log.w("AllOrdersJsonModel model", "AllOrdersJsonModel model");
                for (AllOrdersJsonModel model : resp)
                    Log.e("AllOrdersJsonModel model", model.toString());
                return resp;
            }

            @Override
            public void onFailure(Call<List<AllOrdersJsonModel>> call, Throwable t) {
                Log.e("AllOrdersJsonModel model", "onFailure");
                Log.e("AllOrdersJsonModel model", t.getMessage());
                //if (t instanceof HttpException)

            }
        });
        */
    }

    private void uploadFile(Uri fileUri) {
        // create upload service client
        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);

        // use the FileUtils to get the actual file by uri
        File file = null;//= Utils.getFile(this, fileUri);

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("picture", file.getName(), requestFile);

        // add another part within the multipart request
        String descriptionString = "hello, this is description speaking";
        RequestBody description =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), descriptionString);

        // finally, execute the request
        Call<ResponseBody> call = service.upload(description, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                Log.v("Upload", "success");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
            }
        });
    }

    interface Json {
        @GET("IamEgor/RaspisanieApp/master/json")
        Call<List<AllOrdersModel>> getAllOrders();

        @GET("/IamEgor/TestRest/master/json/order_page.json")
        Call<ApplyOrderModel> getOrder();

    }

    interface FileUploadService {
        @Multipart
        @POST("upload")
        Call<ResponseBody> upload(@Part("description") RequestBody description,
                                  @Part("content") MultipartBody.Part file);

        //TODO https://github.com/square/retrofit/issues/1063
        @Multipart
        @POST("/upload")
        Call<List<String>> uploadImage(@Header("Authorization") String token,
                                       @Header("Handshake") String handshake,
                                       //@Part("file\"; filename=\"902367000083-1.jpg") RequestBody mapFileAndName); //for sending only one image
                                       @PartMap() Map<String, RequestBody> mapFileAndName); //for sending multiple images
    }


}
