package com.example.yegor.testphoto.test;

import com.example.yegor.testphoto.models.json.AllOrdersModel;
import com.example.yegor.testphoto.models.json.ApplyOrderExecutorImageModel;
import com.example.yegor.testphoto.models.json.ApplyOrderExecutorModel;
import com.example.yegor.testphoto.models.json.ApplyOrderModel;
import com.example.yegor.testphoto.models.json.OrderModel;
import com.example.yegor.testphoto.models.json._MoneyModel;
import com.example.yegor.testphoto.models.json._PlaceModel;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

public class TestExtractor {

    public static void main(String[] args) {

        Gson gson = new Gson();

        OrderModel orderModel = new OrderModel.Builder()
                .setOrderId(1L)

                .setCategories(Arrays.asList("Black and White", "Fashion", "Journalism"))
                .setQuantity(8)
                .setMinHeight(1280)
                .setMinWidth(720)
                .setDigitalProcessing(false)
                .setAdditional(
                        "There are many variations of passages of Lorem Ipsum available, but the " +
                                "majority have suffered alteration in some form, by injected humour, or randomised " +
                                "words which don't look even slightly believable. If you are going to use a passage " +
                                "of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in ")

                .setPostTime(153515152L)
                .setDeadline(153715152L)

                .setPrice(new _MoneyModel(25.5f, "USD", "$"))
                .setPlace(
                        new _PlaceModel(
                                "ChIJacr26xG2xUcRkZUjD0FSNgk",
                                "Van der Krukwater 12, 2497 ZT Den Haag, Нидерланды",
                                "Gjvtextiles B.V.",
                                new LatLng(52.036918799999995, 4.373959699999999)))
                .create();


        System.out.println(gson.toJson(orderModel));
        System.out.println();


        AllOrdersModel allOrdersModel = new AllOrdersModel.Builder()
                .setOwnerId(1L)
                .setUrl("http://fwewf.com")
                .setCategories(new String[]{"AA", "AB", "AV", "CA", "V"})
                .setPostTime(153515152L)
                .setDeadline(153715152L)
                .setPrice(new _MoneyModel(25.5f, "USD", "$"))
                .setPlace(
                        new _PlaceModel(
                                "ChIJacr26xG2xUcRkZUjD0FSNgk",
                                "Van der Krukwater 12, 2497 ZT Den Haag, Нидерланды",
                                "Gjvtextiles B.V.",
                                new LatLng(52.036918799999995, 4.373959699999999)))
                .create();

        System.out.println(gson.toJson(allOrdersModel));
        System.out.println();

        System.out.println(gson.toJson(getOrderPageModel()));

    }


    public static ApplyOrderModel getOrderPageModel() {

        ApplyOrderModel model = new ApplyOrderModel.Builder().create();

        ArrayList<ApplyOrderExecutorModel> executorJsonModels = new ArrayList<ApplyOrderExecutorModel>() {{

            add(new ApplyOrderExecutorModel.Builder()
                    .setId(1)
                    .setName("De Niro")
                    .setAvatar("http://img.dummy-image-generator.com/people/dummy-400x400-RobertDeNiro.jpg")
                    .setImages(new ArrayList<ApplyOrderExecutorImageModel>() {{
                        add(
                                new ApplyOrderExecutorImageModel.Builder()
                                        .setId(1)
                                        .setUrl("http://dummy-images.com/abstract/dummy-683x1024-Bottles.jpg")
                                        .setWidth(683)
                                        .setHeight(1024)
                                        .setThumbnail("http://img.dummy-image-generator.com/abstract/dummy-67x100-Bottles.jpg")
                                        .setDominantColor(0x93979C)
                                        .setOwnerId(1)
                                        .create()
                        );
                        add(
                                new ApplyOrderExecutorImageModel.Builder()
                                        .setId(3)
                                        .setUrl("http://dummy-image-generator.com/abstract/dummy-4000x3000-Stripes.jpg")
                                        .setWidth(4000)
                                        .setHeight(3000)
                                        .setThumbnail("http://img.dummy-image-generator.com/abstract/dummy-100x75-Stripes.jpg")
                                        .setDominantColor(0x232938)
                                        .setOwnerId(1)
                                        .create()
                        );
                        add(new ApplyOrderExecutorImageModel.Builder()
                                .setId(6)
                                .setUrl("http://dummy-images.com/abstract/dummy-540x960-Goemetry.jpg")
                                .setWidth(540)
                                .setHeight(960)
                                .setThumbnail("http://img.dummy-image-generator.com/abstract/dummy-54x96-Goemetry.jpg")
                                .setDominantColor(0x0A0D0C)
                                .setOwnerId(1)
                                .create()
                        );
                        add(new ApplyOrderExecutorImageModel.Builder()
                                .setId(9)
                                .setUrl("http://dummy-images.com/animals/dummy-1000x1000-Jellyfish.jpg")
                                .setWidth(1000)
                                .setHeight(1000)
                                .setThumbnail("http://img.dummy-image-generator.com/animals/dummy-100x100-Jellyfish.jpg")
                                .setDominantColor(0x484A4C)
                                .setOwnerId(1)
                                .create()
                        );
                        add(new ApplyOrderExecutorImageModel.Builder()
                                .setId(12)
                                .setUrl("http://dummy-images.com/animals/dummy-926x1500-Gull.jpg")
                                .setWidth(926)
                                .setHeight(1500)
                                .setThumbnail("http://img.dummy-image-generator.com/animals/dummy-62x100-Gull.jpg")
                                .setDominantColor(0xB7B8B7)
                                .setOwnerId(1)
                                .create()
                        );
                        add(new ApplyOrderExecutorImageModel.Builder()
                                .setId(15)
                                .setUrl("http://dummy-images.com/animals/dummy-840x1360-MarrusOrthocanna.jpg")
                                .setWidth(840)
                                .setHeight(1360)
                                .setThumbnail("http://img.dummy-image-generator.com/animals/dummy-62x100-MarrusOrthocanna.jpg")
                                .setDominantColor(0x060708)
                                .setOwnerId(1)
                                .create()
                        );
                        add(new ApplyOrderExecutorImageModel.Builder()
                                .setId(18)
                                .setUrl("http://dummy-images.com/animals/dummy-1760x990-WhiteTiger.jpg")
                                .setWidth(1760)
                                .setHeight(990)
                                .setThumbnail("http://img.dummy-image-generator.com/animals/dummy-100x56-WhiteTiger.jpg")
                                .setDominantColor(0x242428)
                                .setOwnerId(1)
                                .create()
                        );
                        add(new ApplyOrderExecutorImageModel.Builder()
                                .setId(21)
                                .setUrl("http://dummy-images.com/animals/dummy-1360x840-Impala.jpg")
                                .setWidth(1360)
                                .setHeight(840)
                                .setThumbnail("http://img.dummy-image-generator.com/animals/dummy-100x62-Impala.jpg")
                                .setDominantColor(0xB49C83)
                                .setOwnerId(1)
                                .create()
                        );
                    }}).create());

            add(new ApplyOrderExecutorModel.Builder()
                    .setId(2)
                    .setName("Cesar")
                    .setAvatar("http://img.dummy-image-generator.com/people/dummy-400x400-Centurion.jpg")
                    .setImages(new ArrayList<ApplyOrderExecutorImageModel>() {{
                        add(new ApplyOrderExecutorImageModel.Builder()
                                .setId(2)
                                .setUrl("http://dummy-images.com/buildings/dummy-1000x1000-Entrance.jpg")
                                .setWidth(1000)
                                .setHeight(1000)
                                .setThumbnail("http://img.dummy-image-generator.com/buildings/dummy-100x100-Entrance.jpg")
                                .setDominantColor(0xCCC4C3)
                                .setOwnerId(2)
                                .create());
                        add(new ApplyOrderExecutorImageModel.Builder()
                                .setId(72)
                                .setUrl("http://dummy-images.com/nature/dummy-926x1500-Ocean.jpg")
                                .setWidth(926)
                                .setHeight(1500)
                                .setThumbnail("http://img.dummy-image-generator.com/nature/dummy-62x100-Ocean.jpg")
                                .setDominantColor(0xC7DCF7)
                                .setOwnerId(2)
                                .create()
                        );

                    }}).create());

        }};

        model.orderId = 1;

        model.quantity = 8;
        model.minHeight = 1250;
        model.minWidth = 1250;
        model.digitalProcessing = false;
        model.additional = "There are many variations of passages of Lorem Ipsum available, but the " +
                "majority have suffered alteration in some form, by injected humour, or randomised " +
                "words which don't look even slightly believable. If you are going to use a passage " +
                "of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in " +
                "the middle of text. AllOrdersJsonModel the Lorem Ipsum generators on the Internet tend to repeat " +
                "predefined chunks as necessary, making this the first true generator on the " +
                "Internet. It uses a dictionary of over 200 Latin words, combined with a handful " +
                "of model sentence structures, to generate Lorem Ipsum which looks reasonable. " +
                "The generated Lorem Ipsum is therefore always free from repetition, injected " +
                "humour, or non-characteristic words etc.";

        model.postTime = 1456320384;
        model.deadline = 1456520384;


        model.price = new _MoneyModel(25.3f, "USD", "$");
        model.place = new _PlaceModel(
                "ChIJacr26xG2xUcRkZUjD0FSNgk",
                "Van der Krukwater 12, 2497 ZT Den Haag, Нидерланды",
                "Gjvtextiles B.V.",
                new LatLng(52.036918799999995, 4.373959699999999));

        model.executors = executorJsonModels;

        return model;
    }

}
