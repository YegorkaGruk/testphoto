package com.example.yegor.testphoto;

import android.app.Application;
import android.content.Context;

public class App extends Application {//MultiDexApplication {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
        //VKSdk.initialize(getApplicationContext());
        //FacebookSdk.sdkInitialize(getApplicationContext());
    }

}
