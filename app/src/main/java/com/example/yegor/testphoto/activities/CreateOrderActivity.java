package com.example.yegor.testphoto.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.models.json._PlaceModel;
import com.example.yegor.testphoto.test.PlaceArrayAdapter;
import com.example.yegor.testphoto.views.MaterialAutoComplete;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.rey.material.app.Dialog;


public class CreateOrderActivity extends AbstractToolbarButtonsActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    protected static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final String LOG_TAG = CreateOrderActivity.class.getSimpleName();
    private static final long CLICK_DELAY = 300L;
    private final int PLACE_PICKER_REQUEST = 1;
    protected boolean isPressed = false;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private MaterialAutoComplete mAutocompleteTextView;

    private Dialog mDialog;

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = places -> {
        if (!places.getStatus().isSuccess()) {
            Log.e(LOG_TAG, "Place query did not complete. Error: " +
                    places.getStatus().toString());
            return;
        }
        // Selecting the first object buffer.
        final Place place = places.get(0);
        CharSequence attributions = places.getAttributions();

        _PlaceModel placeJson = new _PlaceModel(place.getId(), place.getAddress(), place.getName(), place.getLatLng());
        Toast.makeText(CreateOrderActivity.this, placeJson.toString(), Toast.LENGTH_LONG).show();
        Log.w("toastMsg", placeJson.toString());

        if (attributions != null)
            Log.w("attributions", attributions.toString());

    };

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            Log.i(LOG_TAG, "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_order);

        initToolbar();

        mAutocompleteTextView = (MaterialAutoComplete) findViewById(R.id.place);


        mDialog = new Dialog(this)
                .contentView(R.layout.dialog)
                .cancelable(false);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);

        mAutocompleteTextView.setThreshold(3);

        mAutocompleteTextView.setOnItemClickListener(mAutocompleteClickListener);

        mPlaceArrayAdapter = new PlaceArrayAdapter(this, android.R.layout.simple_list_item_1,
                BOUNDS_MOUNTAIN_VIEW, null);
        mAutocompleteTextView.setAdapter(mPlaceArrayAdapter);

        mAutocompleteTextView.setAlternativeListener(v -> {
            if (!isPressed) {
                try {
                    mDialog.show();
                    //progressDialog.show();
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException e) {
                    Log.e(CreateOrderActivity.class.getSimpleName(), e.getMessage());
                }
            }
            isPressed = true;
            new Handler().postDelayed(() -> isPressed = false, CLICK_DELAY);
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {

        mDialog.hide();

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mPlaceArrayAdapter.setGoogleApiClient(null);
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                _PlaceModel placeJson = new _PlaceModel(place.getId(), place.getAddress(), place.getName(), place.getLatLng());
                Toast.makeText(this, placeJson.toString(), Toast.LENGTH_LONG).show();
                Log.w("toastMsg", placeJson.toString());
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbar_action_done:
                Toast.makeText(this, "CreateOrderActivity action_done", Toast.LENGTH_SHORT).show();
                break;

            case R.id.toolbar_action_cancel:
                NavUtils.navigateUpFromSameTask(this);
                Toast.makeText(this, "CreateOrderActivity action_cancel", Toast.LENGTH_SHORT).show();
                break;

        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }


}
