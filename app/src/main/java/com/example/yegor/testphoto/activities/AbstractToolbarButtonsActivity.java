package com.example.yegor.testphoto.activities;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.utils.Utils;

public abstract class AbstractToolbarButtonsActivity extends AppCompatActivity implements View.OnClickListener {

    protected void initToolbar() {

        Utils.setStatusBarColorIfPossible(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        View actionBarButtons = getLayoutInflater().inflate(R.layout.item_toolbar_buttons3, null);

        View cancelActionView = actionBarButtons.findViewById(R.id.toolbar_action_cancel);
        cancelActionView.setOnClickListener(this);

        View doneActionView = actionBarButtons.findViewById(R.id.toolbar_action_done);
        doneActionView.setOnClickListener(this);

        toolbar.addView(actionBarButtons, Toolbar.LayoutParams.MATCH_PARENT, Toolbar.LayoutParams.MATCH_PARENT);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.toolbar_action_done:
                throw new RuntimeException("You need to implement toolbar buttons");

            case R.id.toolbar_action_cancel:
                throw new RuntimeException("You need to implement toolbar buttons");

        }

    }

}
