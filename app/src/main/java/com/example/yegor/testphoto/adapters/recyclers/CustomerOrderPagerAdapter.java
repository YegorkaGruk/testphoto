package com.example.yegor.testphoto.adapters.recyclers;


import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alexvasilkov.gestures.commons.RecyclePagerAdapter;
import com.alexvasilkov.gestures.views.GestureImageView;
import com.bumptech.glide.Glide;
import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.models.json.AbstractModel;
import com.example.yegor.testphoto.models.json.ApplyOrderExecutorImageModel;
import com.example.yegor.testphoto.utils.GestureSettingsSetupListener;
import com.example.yegor.testphoto.utils.glide.GlideHelper;

import java.util.List;

public class CustomerOrderPagerAdapter extends RecyclePagerAdapter<CustomerOrderPagerAdapter.ViewHolder> {

    private static final long PROGRESS_DELAY = 300L;

    private final ViewPager mViewPager;
    private List<AbstractModel> models;
    private GestureSettingsSetupListener mSetupListener;

    private boolean mActivated;

    private int headerPos;

    public CustomerOrderPagerAdapter(ViewPager viewPager) {
        mViewPager = viewPager;
    }

    public static GestureImageView getImage(RecyclePagerAdapter.ViewHolder holder) {
        return ((ViewHolder) holder).image;
    }

    public int getHeaderPos() {
        return headerPos;
    }

    public ApplyOrderExecutorImageModel getPhoto(int pos) {
        return models == null || pos < 0 || pos >= models.size() ? null : (ApplyOrderExecutorImageModel) models.get(pos - 1);
    }

    public void setPhotos(List<AbstractModel> models, int headerPos) {
        this.models = models;
        this.headerPos = headerPos;
        notifyDataSetChanged();
    }

    public void setSetupListener(GestureSettingsSetupListener listener) {
        mSetupListener = listener;
    }

    /**
     * To prevent ViewPager from holding heavy views (with bitmaps)  while it is not showing
     * we may just pretend there are no items in this adapter ("activate" = false).
     * But once we need to run opening animation we should "activate" this adapter again.<br/>
     * Adapter is not activated by default.
     */
    public void setActivated(boolean activated) {

        if (mActivated != activated) {
            mActivated = activated;
            notifyDataSetChanged();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup container) {

        final ViewHolder holder = new ViewHolder(container);

        holder.image.getController().getSettings().setFillViewport(true).setMaxZoom(3f);//TODO setMaxZoom
        holder.image.getController().enableScrollInViewPager(mViewPager);
        holder.image.getPositionAnimator().addPositionUpdateListener(
                (state, isLeaving) -> holder.progress.setVisibility(state == 1f ? View.VISIBLE : View.INVISIBLE));

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        if (mSetupListener != null) {
            mSetupListener.onSetupGestureView(holder.image);
        }

        // Temporary disabling touch controls
        if (!holder.gesturesDisabled) {
            holder.image.getController().getSettings().disableGestures();
            holder.gesturesDisabled = true;
        }

        holder.progress.animate().setStartDelay(PROGRESS_DELAY).alpha(1f);

        ApplyOrderExecutorImageModel model = (ApplyOrderExecutorImageModel) models.get(position);

        // Loading image
        GlideHelper.loadFull(model, holder.image,
                new GlideHelper.ImageLoadingListener() {
                    @Override
                    public void onLoaded() {
                        holder.progress.animate().cancel();
                        holder.progress.animate().alpha(0f);
                        // Re-enabling touch controls
                        if (holder.gesturesDisabled) {
                            holder.image.getController().getSettings().enableGestures();
                            holder.gesturesDisabled = false;
                        }
                    }

                    @Override
                    public void onFailed() {
                        holder.progress.animate().alpha(0f);
                    }

                });

    }

    @Override
    public int getCount() {
        return !mActivated || models == null ? 0 : models.size();
    }

    @Override
    public void onRecycleViewHolder(@NonNull ViewHolder holder) {
        super.onRecycleViewHolder(holder);

        if (holder.gesturesDisabled) {
            holder.image.getController().getSettings().enableGestures();
            holder.gesturesDisabled = false;
        }

        Glide.clear(holder.image);

        holder.progress.animate().cancel();
        holder.progress.setAlpha(0f);

        holder.image.setImageDrawable(null);
    }

    static class ViewHolder extends RecyclePagerAdapter.ViewHolder {

        public final GestureImageView image;
        public final View progress;

        public boolean gesturesDisabled;

        public ViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_full_image, parent, false));
            image = (GestureImageView) itemView.findViewById(R.id.full_image);
            progress = itemView.findViewById(R.id.full_progress);
        }

    }

}
