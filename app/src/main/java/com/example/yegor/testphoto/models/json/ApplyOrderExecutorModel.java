package com.example.yegor.testphoto.models.json;

import java.util.List;

public class ApplyOrderExecutorModel extends AbstractModel {

    public int id;
    public String name;
    public String avatar;

    public List<ApplyOrderExecutorImageModel> images;

    public ApplyOrderExecutorModel(int id, String name, String avatar, List<ApplyOrderExecutorImageModel> images) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.images = images;
    }

    @Override
    public String toString() {
        return "ApplyOrderExecutorJsonModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", images=" + images +
                '}';
    }

    public static class Builder {

        private int id;
        private String name;
        private String avatar;
        private List<ApplyOrderExecutorImageModel> images;

        public Builder setId(int id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setAvatar(String avatar) {
            this.avatar = avatar;
            return this;
        }

        public Builder setImages(List<ApplyOrderExecutorImageModel> images) {
            this.images = images;
            return this;
        }

        public ApplyOrderExecutorModel create() {
            return new ApplyOrderExecutorModel(id, name, avatar, images);
        }

    }

}
