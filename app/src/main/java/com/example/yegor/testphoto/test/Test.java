package com.example.yegor.testphoto.test;

import com.example.yegor.testphoto.models.json.AllOrdersModel;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Test {

    public static final Comparator<AllOrdersModel> PRICE_COMPARE = (lhs, rhs) -> {

        if ((lhs.price == null && rhs.price == null))
            return 0;

        if (lhs.price != null && lhs.price.amount > 0 && (rhs.price == null || rhs.price.amount == 0))
            return -1;

        if (rhs.price != null && rhs.price.amount > 0 && (lhs.price == null || lhs.price.amount == 0))
            return 1;

        if (lhs.price == null || rhs.price == null)
            throw new RuntimeException("lhs.price == null || rhs.price == null");

        return lhs.price.amount > rhs.price.amount ? -1 : 1;

    };
    public static final Comparator<AllOrdersModel> DEADLINE_COMPARE
            = (lhs, rhs) -> lhs.deadline >= rhs.deadline ? 1 : -1;
    public static final Comparator<AllOrdersModel> POST_TIME_COMPARE
            = (lhs, rhs) -> lhs.postTime >= rhs.postTime ? 1 : -1;
    public static final Comparator<AllOrdersModel> DISTANCE_COMPARE = (lhs, rhs) -> {
        //TODO distance compare
        throw new UnsupportedOperationException();
    };

    public static void main(String[] args) throws FileNotFoundException {

        List<AllOrdersModel> list = getList();

        System.out.println(toJson(list));

        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        Collections.sort(list, POST_TIME_COMPARE);
        System.out.println(toJson(list));

        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        Collections.sort(list, DEADLINE_COMPARE);
        System.out.println(toJson(list));


        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        Collections.sort(list, PRICE_COMPARE);
        System.out.println(toJson(list));

    }

    private static List<AllOrdersModel> getList() throws FileNotFoundException {

        Gson gson = new Gson();

        BufferedReader br = new BufferedReader(
                new FileReader("/home/yegor/Документы/all_orders.json"));

        return Arrays.asList(gson.fromJson(br, AllOrdersModel[].class));

        //return gson.fromJson(br, new TypeToken<AllOrdersModel>() {}.getType());
    }

    private static String toJson(Object o) {
        return new Gson().toJson(o);
    }

}
