package com.example.yegor.testphoto.test;

import com.example.yegor.testphoto.App;
import com.example.yegor.testphoto.utils.ZipUtil;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class TestRest {

    public void map() throws IOException {

        String[] files = new String[]{
                "file:///android_asset/logo.png",
                "file:///android_asset/logo.png",
                "file:///android_asset/logo.png"};

        RequestBody fileBody = RequestBody.create(
                MediaType.parse("application/zip"),
                ZipUtil.zip(files, App.getContext().getFilesDir().getAbsolutePath()));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://raw.githubusercontent.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WebServiceApi service = retrofit.create(WebServiceApi.class);

        Call<OutputReturn> data = service.uploadImage("", "", fileBody);

    }

    interface WebServiceApi {

        String TOKEN = "authenticity_token";

        @Multipart
        @POST("image/upload")
        Call<OutputReturn> uploadImage(@Header(TOKEN) String token,
                                       @Part("userId") String userId,
                                       @Part("file\"; filename=\"images") RequestBody file);
    }

    class OutputReturn {
        String message;
    }

}
