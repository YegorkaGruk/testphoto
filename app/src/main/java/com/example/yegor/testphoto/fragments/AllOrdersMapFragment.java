package com.example.yegor.testphoto.fragments;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.loaders.common.ContentWrapper;
import com.example.yegor.testphoto.models.json.AllOrdersModel;
import com.example.yegor.testphoto.utils.MapUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;
import java.util.List;

public class AllOrdersMapFragment extends AllOrdersAbstractFragment implements
        OnMapReadyCallback {

    Boolean Is_MAP_Moveable = false;
    private GoogleMap mMap;
    private MapView mapView;

    private List<AllOrdersModel> models;
    private List<Marker> markers;

    private double latitude;
    private double longitude;
    private List<LatLng> val = new ArrayList<>();
    private Projection projection;
    private Polygon polygon;

    public AllOrdersMapFragment() {
        super(R.layout.fragment_all_orders_map);
    }

    public static AllOrdersMapFragment newInstance() {
        return new AllOrdersMapFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        mapView = (MapView) rootView.findViewById(R.id.map);
        FrameLayout fram_map = (FrameLayout) rootView.findViewById(R.id.fram_map);
        Button btn_draw_State = (Button) rootView.findViewById(R.id.btn_draw_State);

        btn_draw_State.setOnClickListener(v -> Is_MAP_Moveable = !Is_MAP_Moveable);

        fram_map.setOnTouchListener((v, event) -> {

            Log.w("setOnTouchListener", "Is_MAP_Moveable = " + Is_MAP_Moveable);

            if (!Is_MAP_Moveable)
                return Is_MAP_Moveable;

            float x = event.getX();
            float y = event.getY();

            int x_co = Math.round(x);
            int y_co = Math.round(y);

            projection = mMap.getProjection();
            Point x_y_points = new Point(x_co, y_co);

            LatLng latLng = mMap.getProjection().fromScreenLocation(x_y_points);
            latitude = latLng.latitude;

            longitude = latLng.longitude;

            int eventaction = event.getAction();
            switch (eventaction) {
                case MotionEvent.ACTION_DOWN:
                    // finger touches the screen
                    val.clear();
                    setAllMarkersInvisible();

                    val.add(new LatLng(latitude, longitude));
                    break;
                case MotionEvent.ACTION_MOVE:
                    // finger moves on the screen
                    val.add(new LatLng(latitude, longitude));
                    Draw_Map();

                    break;
                //какой-то поток ожидания
                case MotionEvent.ACTION_UP:
                    // finger leaves the screen
                    setInnerMarkersVisible();
                    break;
            }

            return Is_MAP_Moveable;
        });

        mapView.onCreate(savedInstanceState);
        // Gets to GoogleMap from the MapView and does initialization stuff
        mapView.getMapAsync(this);

        return rootView;
    }

    public void Draw_Map() {
        PolygonOptions rectOptions = new PolygonOptions();
        rectOptions.addAll(val);
        rectOptions.strokeColor(Color.BLUE);
        rectOptions.strokeWidth(7);
        rectOptions.fillColor(Color.CYAN);
        if (polygon != null)
            polygon.remove();
        polygon = mMap.addPolygon(rectOptions);
        polygon.getHoles();
    }

    private void setAllMarkersInvisible() {
        for (Marker marker : markers) marker.setVisible(false);
    }

    private void setInnerMarkersVisible() {
        for (Marker marker : markers)
            marker.setVisible(MapUtil.containsLocation(marker.getPosition(), val, false));
    }

    // Add the mapView lifecycle to the activity's lifecycle methods
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;

        mMap.setMyLocationEnabled(true);
        mMap.setBuildingsEnabled(true);
        //map.getUiSettings().setZoomControlsEnabled(true);

        if (models != null) {
            markers = new ArrayList<>(models.size());
            for (AllOrdersModel model : models)
                if (model.place != null)
                    markers.add(mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(model.place.latitude, model.place.longitude))
                            .title(model.place.name)));
            models = null;
            Log.w("onMapReady", "markers != null");
        }

    }

    @Override
    protected void onDataReceived(List<AllOrdersModel> models) {

        if (mMap != null) {
            markers = new ArrayList<>(models.size());
            //ewfwe
            for (AllOrdersModel model : models)
                if (model.place != null)
                    markers.add(mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(model.place.latitude, model.place.longitude))
                            .title(model.place.name)));
            Log.w("onLoadFinished", "mMap != null");
        } else {
            this.models = models;
            Log.w("onLoadFinished", "mMap == null");
        }

    }

    @Override
    public void onLoaderReset(Loader<ContentWrapper<List<AllOrdersModel>>> loader) {
        models = null;
    }

}