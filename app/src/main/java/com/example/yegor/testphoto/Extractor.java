package com.example.yegor.testphoto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Extractor {

    private Matcher matcher;

    private Pattern pattern1 = Pattern.compile("\\d+x\\d+");
    private Pattern pattern2 = Pattern.compile("\\d+");

    private String result;

    private int[] params = new int[2];

    public double extractRatio(String s) {
        int[] ints = extractArray(s);
        return ints[0] / (double) ints[1];
    }

    public int[] extractArray(String s) {

        matcher = pattern1.matcher(s);

        while (matcher.find())
            result = matcher.group();

        matcher = pattern2.matcher(result);

        for (int i = 0; i < params.length; i++) {

            if (!matcher.find())
                throw new RuntimeException();

            params[i] = Integer.valueOf(matcher.group());

        }

        return params;

    }

    public String format(int[] i) {
        return "Dimensions : " + i[0] + "   " + i[1];
    }

    public String format(String s) {
        return format(extractArray(s));
    }

}