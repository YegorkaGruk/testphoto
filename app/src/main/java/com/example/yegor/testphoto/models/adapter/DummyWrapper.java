package com.example.yegor.testphoto.models.adapter;

import com.example.yegor.testphoto.models.json.AbstractModel;
import com.example.yegor.testphoto.models.json.ApplyOrderExecutorImageModel;
import com.example.yegor.testphoto.models.json.ApplyOrderExecutorModel;
import com.example.yegor.testphoto.models.json.ApplyOrderModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DummyWrapper {

    private List<AbstractModel> models = new ArrayList<>();

    private List<Integer> headersIndexes = new ArrayList<>();
    private Set<Integer> chosenImages = new HashSet<>();

    private int wholeSize;

    public DummyWrapper() {
    }

    public DummyWrapper(ApplyOrderModel jsonModel) {

        List<ApplyOrderExecutorModel> list = jsonModel.executors;

        for (ApplyOrderExecutorModel orderExecutorJsonModel : list) {
            add(orderExecutorJsonModel);
            add(orderExecutorJsonModel.images);
        }

    }

    public int size() {
        return wholeSize;
    }

    public void add(ApplyOrderExecutorModel executor) {
        models.add(executor);
        headersIndexes.add(wholeSize++);
    }

    public void add(List<ApplyOrderExecutorImageModel> images) {
        models.addAll(images);
        wholeSize += images.size();
    }

    public boolean isHeader(int position) {
        return headersIndexes.contains(position);
    }

    @Deprecated
    public AbstractModel get(int position) {
        return models.get(position);
    }

    public ApplyOrderExecutorModel getExecutor(int position) {

        if (!isHeader(position))
            throw new RuntimeException("It's not a header");

        return (ApplyOrderExecutorModel) models.get(position);
    }

    public ApplyOrderExecutorImageModel getImage(int position) {

        //if (isHeader(position))
        //    throw new RuntimeException("It's a header");

        return (ApplyOrderExecutorImageModel) models.get(position);

    }

    @Deprecated
    public int getPosition(AbstractModel model) {
        return models.indexOf(model);
    }

    public int getImagePosition(ApplyOrderExecutorImageModel model) {
        return models.indexOf(model);
    }

    public ApplyOrderExecutorImageModel getImageOldPosition(int newPos, int headerPos) {
        return (ApplyOrderExecutorImageModel) models.get(headerPos + newPos + 1);
    }

    public int getHeaderPosition(int position) {

        List<Integer> temp = new ArrayList<>(headersIndexes);
        temp.add(models.size());

        if (temp.contains(position))
            throw new RuntimeException("This is a header");

        int prev = 0;

        for (Integer integer : temp)
            if (position < integer)
                return prev;
            else
                prev = integer;


        throw new RuntimeException("Smth wrong with arrayList!");
    }

    @Deprecated
    public int getNewPosition(AbstractModel model) {
        return getImageNewPosition(models.indexOf(model));
    }

    public int getImageNewPosition(ApplyOrderExecutorImageModel model) {
        return getImageNewPosition(models.indexOf(model));
    }

    public int getImageNewPosition(int position) {
        return position - getHeaderPosition(position) - 1;
    }

    public List<AbstractModel> getSublist(int position) {

        int[] endpoints = new int[2];

        List<Integer> temp = new ArrayList<>(headersIndexes);
        temp.add(models.size());

        for (Integer integer : temp) {

            endpoints[1] = integer;

            if (endpoints[0] - position < 0 && position - endpoints[1] < 0)
                break;
            else
                endpoints[0] = integer;

        }

        if (endpoints[0] == endpoints[1])
            throw new RuntimeException("Wrong input!");

        return models.subList(endpoints[0] + 1, endpoints[1]);//для того, чтобы оказаться внутри иртервала
    }

}
