package com.example.yegor.testphoto.models.json;

import java.util.Arrays;

public class AllOrdersModel {

    public long ownerId;

    public String url;
    public String[] categories;

    public long postTime;
    public long deadline;

    public _MoneyModel price;
    public _PlaceModel place;

    public AllOrdersModel(long ownerId, String url, String[] categories, long postTime,
                          long deadline, _MoneyModel price, _PlaceModel place) {

        this.ownerId = ownerId;
        this.url = url;
        this.categories = categories;
        this.postTime = postTime;
        this.deadline = deadline;
        this.price = price;
        this.place = place;

    }

    @Override
    public String toString() {
        return "AllOrdersModel{" +
                "ownerId=" + ownerId +
                ", url='" + url + '\'' +
                ", categories=" + Arrays.toString(categories) +
                ", postTime=" + postTime +
                ", deadline=" + deadline +
                ", price=" + price +
                ", place=" + place +
                '}';
    }

    public static class Builder {

        private long ownerId;
        private String url;
        private String[] categories;
        private long postTime;
        private long deadline;
        private _MoneyModel price;
        private _PlaceModel place;

        public Builder setOwnerId(long ownerId) {
            this.ownerId = ownerId;
            return this;
        }

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setCategories(String[] categories) {
            this.categories = categories;
            return this;
        }

        public Builder setPostTime(long postTime) {
            this.postTime = postTime;
            return this;
        }

        public Builder setDeadline(long deadline) {
            this.deadline = deadline;
            return this;
        }

        public Builder setPrice(_MoneyModel price) {
            this.price = price;
            return this;
        }

        public Builder setPlace(_PlaceModel place) {
            this.place = place;
            return this;
        }

        public AllOrdersModel create() {
            return new AllOrdersModel(ownerId, url, categories, postTime, deadline, price, place);
        }

    }

}