package com.example.yegor.testphoto.utils.glide;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.ViewPropertyAnimation;
import com.example.yegor.testphoto.models.json.ApplyOrderExecutorImageModel;

public class GlideHelper {

    private static final ViewPropertyAnimation.Animator ANIMATOR =
            view -> {
                view.setAlpha(0f);
                view.animate().alpha(1f);
            };

    //Todo бессмыслица. В чем отличие от след. метода
    public static void loadThumb(@Nullable ApplyOrderExecutorImageModel model, @NonNull final ImageView image) {
        Glide.with(image.getContext())
                .load(model == null ? null : model.url)//TODO getMediumUrl
                .dontAnimate()
                .thumbnail(Glide.with(image.getContext())
                        .load(model == null ? null : model.thumbnail)
                        .animate(ANIMATOR)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE))
                .into(new GlideDrawableTarget(image));
    }

    public static void loadFull(@NonNull ApplyOrderExecutorImageModel model,
                                @NonNull final ImageView image,
                                @Nullable final ImageLoadingListener listener) {

        final String photoUrl = model.url;

        Glide.with(image.getContext())
                .load(photoUrl)
                .dontAnimate()
                .placeholder(image.getDrawable())
                .thumbnail(Glide.with(image.getContext())
                        .load(model.thumbnail)
                        .animate(ANIMATOR)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .listener(new GlideDrawableListener() {
                    @Override
                    public void onSuccess(String url) {
                        if (url.equals(photoUrl)) {
                            if (listener != null) {
                                listener.onLoaded();
                            }
                        }
                    }

                    @Override
                    public void onFail(String url) {
                        if (listener != null) {
                            listener.onFailed();
                        }
                    }
                })
                .into(new GlideDrawableTarget(image));

    }

    public interface ImageLoadingListener {
        void onLoaded();

        void onFailed();
    }

}