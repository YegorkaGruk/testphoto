package com.example.yegor.testphoto.adapters.recyclers.holders;

public interface ErasableHolder {
    void erase();
}