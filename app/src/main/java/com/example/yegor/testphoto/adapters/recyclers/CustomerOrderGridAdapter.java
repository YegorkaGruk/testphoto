package com.example.yegor.testphoto.adapters.recyclers;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.yegor.testphoto.adapters.recyclers.holders.CustomerOrderHeaderHolder;
import com.example.yegor.testphoto.adapters.recyclers.holders.CustomerOrderImageHolder;
import com.example.yegor.testphoto.models.adapter.DummyWrapper;
import com.example.yegor.testphoto.models.json.AbstractModel;
import com.example.yegor.testphoto.models.json.ApplyOrderExecutorImageModel;

import java.util.List;

public class CustomerOrderGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String GET_IMAGES = "get_images";
    public static final int TYPE_HEADER = 9000;
    public static final int TYPE_IMAGE = 9001;
    private final OnPhotoListener mListener;
    private DummyWrapper models;

    public CustomerOrderGridAdapter(DummyWrapper models, OnPhotoListener mListener) {
        this.models = models;
        this.mListener = mListener;
    }

    public static ImageView getImage(RecyclerView.ViewHolder holder) {
        if (holder instanceof CustomerOrderImageHolder) {
            return ((CustomerOrderImageHolder) holder).getImage();
        } else {
            return null;
        }
    }

    public void setModels(DummyWrapper models) {
        this.models = models;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.e("onCreateViewHolder", "width - " + parent.getWidth() + " height -" + parent.getHeight());

        switch (viewType) {
            case TYPE_HEADER:
                return new CustomerOrderHeaderHolder(parent);
            case TYPE_IMAGE:
                return new CustomerOrderImageHolder(parent);
            default:
                return new CustomerOrderImageHolder(parent);
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {

            case TYPE_HEADER:

                CustomerOrderHeaderHolder holder1 = (CustomerOrderHeaderHolder) holder;

                holder1.setName(models.getExecutor(position).name);
                holder1.setAvatar(models.getExecutor(position).avatar);

                StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
                layoutParams.setFullSpan(true);

                break;

            case TYPE_IMAGE:
                final CustomerOrderImageHolder holder2 = (CustomerOrderImageHolder) holder;

                holder2.setImage(models.getImage(position));
                //holder2.getImage().setTag(models.getSublist(position));
                holder2.setOnImageListener(v -> {
                    int pos = holder2.getAdapterPosition();

                    List<AbstractModel> pagerContent = models.getSublist(pos);
                    int newPosition = pagerContent.indexOf(models.getImage(pos)) + 1;//Первый - это заголовок

                    Log.d("pagerContent", pagerContent.toString());

                    mListener.onPhotoClick(
                            pagerContent,
                            newPosition,
                            models.getImage(pos),
                            models.getHeaderPosition(position));
                });
        }

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (models.isHeader(position))
            return TYPE_HEADER;
        else
            return TYPE_IMAGE;
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder instanceof CustomerOrderImageHolder) {
            Glide.clear(((CustomerOrderImageHolder) holder).getImage());
        }
        //TODO else if (holder instanceof CustomerOrderHeaderHolder)
    }

    public interface OnPhotoListener {
        void onPhotoClick(List<AbstractModel> models, int position, ApplyOrderExecutorImageModel model, int headerPos);
    }

}
