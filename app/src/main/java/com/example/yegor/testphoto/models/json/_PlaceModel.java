package com.example.yegor.testphoto.models.json;

import com.google.android.gms.maps.model.LatLng;

public class _PlaceModel {

    public String id;
    public String address;
    public String name;
    public double latitude;
    public double longitude;

    public _PlaceModel(String id, CharSequence address, CharSequence name, LatLng latLng) {
        this.id = id;
        this.address = address.toString();
        this.name = name.toString();
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
    }

    @Override
    public String toString() {
        return "_PlaceModel{" +
                "id='" + id + '\'' +
                ", address='" + address + '\'' +
                ", name='" + name + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}