package com.example.yegor.testphoto.loaders.common;

public class NoConnectionException extends Exception {

    @Override
    public String getMessage() {
        return "No Internet Connection Exception";
    }

}
