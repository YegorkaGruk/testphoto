package com.example.yegor.testphoto.adapters.recyclers;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yegor.testphoto.App;
import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.models.json.AllOrdersModel;
import com.example.yegor.testphoto.utils.Utils;
import com.rey.material.widget.RelativeLayout;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.shts.android.library.TriangleLabelView;

public class AllOrdersAdapter extends RecyclerView.Adapter<AllOrdersAdapter.ViewHolder> {

    public static final Comparator<AllOrdersModel> PRICE_COMPARE = (lhs, rhs) -> {

        if ((lhs.price == null && rhs.price == null))
            return 0;

        if (lhs.price != null && lhs.price.amount > 0 && (rhs.price == null || rhs.price.amount == 0))
            return 1;

        if (rhs.price != null && rhs.price.amount > 0 && (lhs.price == null || lhs.price.amount == 0))
            return -1;

        if (lhs.price == null || rhs.price == null)
            throw new RuntimeException("lhs.price == null || rhs.price == null");

        return lhs.price.amount > rhs.price.amount ? 1 : -1;

    };

    public static final Comparator<AllOrdersModel> DEADLINE_COMPARE = (lhs, rhs) -> {
        //if (lhs.deadline == rhs.deadline)
        //    return 0;
        return lhs.deadline >= rhs.deadline ? 1 : -1;
    };

    public static final Comparator<AllOrdersModel> POST_TIME_COMPARE
            = (lhs, rhs) -> lhs.postTime >= rhs.postTime ? 1 : -1;

    public static final Comparator<AllOrdersModel> DISTANCE_COMPARE = (lhs, rhs) -> {
        //TODO distance compare
        throw new UnsupportedOperationException();
    };

    private List<AllOrdersModel> models;

    private OnItemClick onItemClick;

    public AllOrdersAdapter(List<AllOrdersModel> models, OnItemClick onItemClick) {
        this.models = models;
        this.onItemClick = onItemClick;
    }

    public void setModels(List<AllOrdersModel> models) {
        this.models = models;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_order, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder itemViewHolder, int position) {
        Log.w("onBindViewHolder", models.get(position).toString());
        itemViewHolder.setShortDescription("\t\t" + TextUtils.join(", ", models.get(position).categories));
        //itemViewHolder.setFullDescription(models.get(position).fullDescription);
        itemViewHolder.setPrice(models.get(position).price != null ?
                models.get(position).price.getMessage() : "Free");//TODO currency
        itemViewHolder.setPostTime(Utils.getTimeStr(models.get(position).postTime));
        itemViewHolder.setDeadline(Utils.getTimeStr(models.get(position).deadline));
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void sort(Comparator<AllOrdersModel> comparator) {
        Toast.makeText(App.getContext(), "comparator - " + comparator.toString(), Toast.LENGTH_SHORT).show();
        Log.w("sort", "comparator - " + comparator.toString());
        Collections.sort(models, comparator);
        notifyDataSetChanged();
    }

    public interface OnItemClick {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private CardView cardView;
        private RelativeLayout container;
        private TriangleLabelView label;
        private TextView shortDescription;
        private TextView fullDescription;
        private TextView price;
        private TextView postTime;
        private TextView deadline;

        public ViewHolder(View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.cv);
            container = (RelativeLayout) itemView.findViewById(R.id.container);
            label = (TriangleLabelView) itemView.findViewById(R.id.label);
            shortDescription = (TextView) itemView.findViewById(R.id.short_description);
            fullDescription = (TextView) itemView.findViewById(R.id.full_description);
            price = (TextView) itemView.findViewById(R.id.price);
            postTime = (TextView) itemView.findViewById(R.id.post_time);
            deadline = (TextView) itemView.findViewById(R.id.deadline);

            if (Utils.isLollipop())
                cardView.setOnClickListener(this);
            else
                container.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onItemClick.onItemClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            Toast.makeText(App.getContext(), "onLongClick", Toast.LENGTH_SHORT).show();
            return true;
        }

        //TODO проверить являеися ли заказ своим
        public void setLabelVisibility(boolean visible) {
            label.setVisibility(visible ? View.VISIBLE : View.GONE);
        }

        public void setShortDescription(String shortDescription) {
            this.shortDescription.setText(shortDescription);
        }

        public void setFullDescription(String fullDescription) {
            this.fullDescription.setText(fullDescription);
        }

        public void setPrice(String price) {
            this.price.setText(price);
        }

        public void setPostTime(String time) {
            postTime.setText(time);
        }

        public void setDeadline(String time) {
            deadline.setText(time);
        }


    }

}