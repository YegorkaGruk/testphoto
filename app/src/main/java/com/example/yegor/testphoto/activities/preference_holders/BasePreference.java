package com.example.yegor.testphoto.activities.preference_holders;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.yegor.testphoto.App;

public abstract class BasePreference {

    public static final String APP_PREFS = "app_prefs";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    static {
        sharedPreferences = App.getContext().getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    protected static void put(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    protected static boolean getBool(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    protected static void put(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    protected static int getInt(String key, int defValue) {
        return sharedPreferences.getInt(key, defValue);
    }

}
