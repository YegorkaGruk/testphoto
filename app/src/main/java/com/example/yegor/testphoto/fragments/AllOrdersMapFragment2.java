package com.example.yegor.testphoto.fragments;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.loaders.common.ContentWrapper;
import com.example.yegor.testphoto.models.json.AllOrdersModel;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class AllOrdersMapFragment2 extends AllOrdersAbstractFragment implements
        OnMapReadyCallback, View.OnTouchListener {

    private GoogleMap mMap;
    private MapView mapView;

    private List<AllOrdersModel> models;
    private List<Marker> markers;


    private GestureDetector mGestureDetector;
    private PolygonOptions mPolygonOptions;
    private PolylineOptions mPolylineOptions;
    private List<LatLng> mLatlngs = new ArrayList<>();
    private View mMapShelterView;
    // flag to differentiate whether user is touching to draw or not
    private boolean mDrawFinished = false;

    public AllOrdersMapFragment2() {
        super(R.layout.fragment_all_orders_map);
    }

    public static AllOrdersMapFragment2 newInstance() {
        return new AllOrdersMapFragment2();
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            return false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        mapView = (MapView) rootView.findViewById(R.id.map);
        mMapShelterView = rootView.findViewById(R.id.fram_map);
        Button btn_draw_State = (Button) rootView.findViewById(R.id.btn_draw_State);
btn_draw_State.setOnClickListener((view) -> drawZone(view));

        mGestureDetector = new GestureDetector(getContext(), new GestureListener());
        mMapShelterView.setOnTouchListener(this);

        mapView.onCreate(savedInstanceState);
        // Gets to GoogleMap from the MapView and does initialization stuff
        mapView.getMapAsync(this);

        return rootView;
    }

    /**
     * Ontouch event will draw poly line along the touch points
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int X1 = (int) event.getX();
        int Y1 = (int) event.getY();
        Point point = new Point();
        point.x = X1;
        point.y = Y1;
        LatLng firstGeoPoint = mMap.getProjection().fromScreenLocation(
                point);
        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                break;

            case MotionEvent.ACTION_MOVE:
                if (mDrawFinished) {
                    X1 = (int) event.getX();
                    Y1 = (int) event.getY();
                    point = new Point();
                    point.x = X1;
                    point.y = Y1;
                    LatLng geoPoint = mMap.getProjection()
                            .fromScreenLocation(point);
                    mLatlngs.add(geoPoint);
                    mPolylineOptions = new PolylineOptions();
                    mPolylineOptions.color(Color.RED);
                    mPolylineOptions.width(3);
                    mPolylineOptions.addAll(mLatlngs);
                    mMap.addPolyline(mPolylineOptions);
                }
                break;
            case MotionEvent.ACTION_UP:
                Log.d("ACTION_UP", "Poinnts array size " + mLatlngs.size());
                mLatlngs.add(firstGeoPoint);
                mMap.clear();
                mPolylineOptions = null;
                mMapShelterView.setVisibility(View.GONE);
                mMap.getUiSettings().setZoomGesturesEnabled(true);
                mMap.getUiSettings().setAllGesturesEnabled(true);
                mPolygonOptions = new PolygonOptions();
                mPolygonOptions.fillColor(Color.GRAY);
                mPolygonOptions.strokeColor(Color.RED);
                mPolygonOptions.strokeWidth(5);
                mPolygonOptions.addAll(mLatlngs);
                mMap.addPolygon(mPolygonOptions);
                mDrawFinished = false;
                break;
        }
        return mGestureDetector.onTouchEvent(event);
    }

    /**
     * Method gets called on tap of draw button, It prepares the screen to draw
     * the polygon
     *
     * @param view
     */

    public void drawZone(View view) {
        mMap.clear();
        mLatlngs.clear();
        mPolylineOptions = null;
        mPolygonOptions = null;
        mDrawFinished = true;
        mMapShelterView.setVisibility(View.VISIBLE);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
    }

    // Add the mapView lifecycle to the activity's lifecycle methods
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;

        mMap.setMyLocationEnabled(true);
        mMap.setBuildingsEnabled(true);
        //map.getUiSettings().setZoomControlsEnabled(true);

        if (models != null) {
            markers = new ArrayList<>(models.size());
            for (AllOrdersModel model : models)
                if (model.place != null)
                    markers.add(mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(model.place.latitude, model.place.longitude))
                            .title(model.place.name)));
            models = null;
            Log.w("onMapReady", "markers != null");
        }

    }

    @Override
    protected void onDataReceived(List<AllOrdersModel> models) {

        if (mMap != null) {
            markers = new ArrayList<>(models.size());
            //ewfwe
            for (AllOrdersModel model : models)
                if (model.place != null)
                    markers.add(mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(model.place.latitude, model.place.longitude))
                            .title(model.place.name)));
            Log.w("onLoadFinished", "mMap != null");
        } else {
            this.models = models;
            Log.w("onLoadFinished", "mMap == null");
        }

    }

    @Override
    public void onLoaderReset(Loader<ContentWrapper<List<AllOrdersModel>>> loader) {
        models = null;
    }

}