package com.example.yegor.testphoto.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.activities.CustomerOrderActivity;
import com.example.yegor.testphoto.adapters.recyclers.AllOrdersAdapter;
import com.example.yegor.testphoto.loaders.common.ContentWrapper;
import com.example.yegor.testphoto.models.json.AllOrdersModel;

import java.util.ArrayList;
import java.util.List;

public class AllOrdersListFragment extends AllOrdersAbstractFragment implements
        AllOrdersAdapter.OnItemClick {

    private static final int CLICK_DELAY = 300;

    private AllOrdersAdapter adapter;
    private RecyclerView rv;

    private boolean isPressed = false;

    public AllOrdersListFragment() {
        super(R.layout.fragment_all_orders);
    }

    public static AllOrdersListFragment newInstance() {
        return new AllOrdersListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        getActivity().findViewById(R.id.by_distance).setOnClickListener(
                v1 -> adapter.sort(AllOrdersAdapter.DISTANCE_COMPARE));
        getActivity().findViewById(R.id.by_price).setOnClickListener(
                v1 -> adapter.sort(AllOrdersAdapter.PRICE_COMPARE));
        getActivity().findViewById(R.id.by_deadline).setOnClickListener(
                v1 -> adapter.sort(AllOrdersAdapter.DEADLINE_COMPARE));
        getActivity().findViewById(R.id.by_post_time).setOnClickListener(
                v1 -> adapter.sort(AllOrdersAdapter.POST_TIME_COMPARE));

        rv = (RecyclerView) rootView.findViewById(R.id.rv);
        rv.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(mLayoutManager);

        adapter = new AllOrdersAdapter(new ArrayList<>(0), this);
        rv.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onItemClick(View view, int position) {

        if (!isPressed) {
            Intent intent = new Intent(getActivity(), CustomerOrderActivity.class);
            startActivity(intent);
        }

        isPressed = true;

        new Handler().postDelayed(() -> isPressed = false, CLICK_DELAY);
    }

    @Override
    protected void onDataReceived(List<AllOrdersModel> models) {
        adapter.setModels(models);
    }

    @Override
    public void onLoaderReset(Loader<ContentWrapper<List<AllOrdersModel>>> loader) {
        adapter.setModels(new ArrayList<>(0));
    }

}
