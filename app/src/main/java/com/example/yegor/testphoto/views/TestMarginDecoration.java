package com.example.yegor.testphoto.views;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.yegor.testphoto.R;

public class TestMarginDecoration extends RecyclerView.ItemDecoration {

    private int margin;

    public TestMarginDecoration(Context context) {
        margin = context.getResources().getDimensionPixelSize(R.dimen.image_item_margin);
    }

    @Override
    public void getItemOffsets(
            Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(margin, margin, margin, margin);
    }
}