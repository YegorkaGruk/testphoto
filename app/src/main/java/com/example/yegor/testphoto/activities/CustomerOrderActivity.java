package com.example.yegor.testphoto.activities;

import android.annotation.SuppressLint;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alexvasilkov.gestures.animation.ViewPositionAnimator;
import com.alexvasilkov.gestures.commons.DepthPageTransformer;
import com.alexvasilkov.gestures.commons.RecyclePagerAdapter;
import com.alexvasilkov.gestures.transition.ViewsTracker;
import com.alexvasilkov.gestures.transition.ViewsTransitionAnimator;
import com.alexvasilkov.gestures.transition.ViewsTransitionBuilder;
import com.example.yegor.testphoto.R;
import com.example.yegor.testphoto.adapters.recyclers.CustomerOrderGridAdapter;
import com.example.yegor.testphoto.adapters.recyclers.CustomerOrderPagerAdapter;
import com.example.yegor.testphoto.loaders.AbstractJsonLoader;
import com.example.yegor.testphoto.loaders.common.ContentWrapper;
import com.example.yegor.testphoto.loaders.common.Json;
import com.example.yegor.testphoto.models.adapter.DummyWrapper;
import com.example.yegor.testphoto.models.json.AbstractModel;
import com.example.yegor.testphoto.models.json.ApplyOrderExecutorImageModel;
import com.example.yegor.testphoto.models.json.ApplyOrderModel;
import com.example.yegor.testphoto.utils.Utils;
import com.rey.material.widget.CheckBox;

import java.util.List;

import retrofit2.Call;

public class CustomerOrderActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<ContentWrapper<ApplyOrderModel>>,
        ViewPositionAnimator.PositionUpdateListener,
        CustomerOrderGridAdapter.OnPhotoListener {

    private RecyclerView rv;
    private ViewPager viewPager;

    private Toolbar toolbar;
    private Toolbar pagerToolbar;

    private View toolbarShadow;
    private View pagerBackground;

    private CustomerOrderGridAdapter mGridAdapter;
    private CustomerOrderPagerAdapter mPagerAdapter;

    private ViewsTransitionAnimator<AbstractModel> mAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_order);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarShadow = findViewById(R.id.toolbar_shadow);

        initGrid();
        initPager();

        Utils.setStatusBarColorIfPossible(this);
        toolbar.setTitle(R.string.customer_order_activity_title);

        getLoaderManager().initLoader(0, null, this).forceLoad();
    }

    @Override
    public void onBackPressed() {
        if (mAnimator == null || mAnimator.isLeaving())
            super.onBackPressed();
        else
            mAnimator.exit(true);
    }

    @Override
    public void onPhotoClick(List<AbstractModel> models, int position, ApplyOrderExecutorImageModel model, int headerPos) {
        toolbarShadow.setVisibility(View.INVISIBLE);
        mPagerAdapter.setPhotos(models, headerPos);
        mPagerAdapter.setActivated(true);
        mAnimator.enter(model, true);
    }

    @Override
    public void onPositionUpdate(float state, boolean isLeaving) {
        pagerBackground.setVisibility(state == 0f ? View.INVISIBLE : View.VISIBLE);
        pagerBackground.getBackground().setAlpha((int) (255 * state));

        toolbar.setVisibility(state == 1f ? View.INVISIBLE : View.VISIBLE);
        toolbar.setAlpha((float) Math.sqrt(1d - state)); // Slow down toolbar animation

        pagerToolbar.setVisibility(state == 0f ? View.INVISIBLE : View.VISIBLE);
        pagerToolbar.setAlpha(state);

        if (isLeaving && state == 0f) {
            mPagerAdapter.setActivated(false);
        }
    }

    private void initGrid() {
        // Setting up images grid
        final int cols = getResources().getInteger(R.integer.images_grid_columns);

        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new StaggeredGridLayoutManager(cols, StaggeredGridLayoutManager.VERTICAL));
        rv.setItemAnimator(new DefaultItemAnimator());

        mGridAdapter = new CustomerOrderGridAdapter(new DummyWrapper(), this);
        rv.setAdapter(mGridAdapter);

    }

    @SuppressLint("PrivateResource")
    private void initPager() {
        // Setting up pager views
        viewPager = (ViewPager) findViewById(R.id.pager);
        pagerToolbar = (Toolbar) findViewById(R.id.full_toolbar);
        pagerBackground = findViewById(R.id.full_background);

        mPagerAdapter = new CustomerOrderPagerAdapter(viewPager);

        viewPager.setAdapter(mPagerAdapter);
        viewPager.setPageTransformer(true, new DepthPageTransformer());

        pagerToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        pagerToolbar.setNavigationOnClickListener(v -> onBackPressed());

        RelativeLayout relativeLayout = new RelativeLayout(this);
        CheckBox checkBox = (CheckBox) getLayoutInflater().inflate(R.layout.test_item_chb, relativeLayout, false);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) checkBox.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        checkBox.setLayoutParams(params);
        relativeLayout.addView(checkBox);

        pagerToolbar.addView(relativeLayout);

    }

    private void initAnimator(final DummyWrapper wrapper) {

        mAnimator = new ViewsTransitionBuilder<AbstractModel>()

                .fromRecyclerView(rv,
                        new ViewsTracker<AbstractModel>() {
                            @Override
                            public int getPositionForId(@NonNull AbstractModel model) {
                                return wrapper.getPosition(model);
                            }

                            @Override
                            public AbstractModel getIdForPosition(int position) {
                                return wrapper.get(position);
                            }

                            @Override
                            public View getViewForPosition(int position) {
                                RecyclerView.ViewHolder holder =
                                        rv.findViewHolderForLayoutPosition(position);
                                return holder == null ? null : CustomerOrderGridAdapter.getImage(holder);
                            }
                        }
                )
                .intoViewPager(viewPager, new ViewsTracker<AbstractModel>() {
                            @Override
                            public int getPositionForId(@NonNull AbstractModel model) {
                                return wrapper.getNewPosition(model);
                            }

                            @Override
                            public AbstractModel getIdForPosition(int newPosition) {
                                return wrapper.getImageOldPosition(newPosition, mPagerAdapter.getHeaderPos());
                            }

                            @Override
                            public View getViewForPosition(int position) {
                                RecyclePagerAdapter.ViewHolder holder = mPagerAdapter.getViewHolder(
                                        position);
                                return holder == null ? null : CustomerOrderPagerAdapter.getImage(holder);
                            }
                        }
                ).build();

        mAnimator.addPositionUpdateListener(this);
        mAnimator.setReadyListener(id -> {
            // Setting image drawable from 'from' view to 'to' to prevent flickering
            ImageView from = (ImageView) mAnimator.getFromView();
            ImageView to = (ImageView) mAnimator.getToView();
            if (to.getDrawable() == null) {
                to.setImageDrawable(from.getDrawable());
            }
        });
    }

    //----------------------------------------------------------------------------------------------
    @Override
    public Loader<ContentWrapper<ApplyOrderModel>> onCreateLoader(int id, Bundle args) {
        return new AbstractJsonLoader<ApplyOrderModel>(this) {
            @Override
            public Call<ApplyOrderModel> getMethod(Json service) {
                Log.w(TAG, "CustomerOrderActivity onCreateLoader getMethod");
                return service.getOrder();
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<ContentWrapper<ApplyOrderModel>> loader, ContentWrapper<ApplyOrderModel> data) {
        DummyWrapper models = new DummyWrapper(data.getContent());
        mGridAdapter.setModels(models);
        initAnimator(models);
    }

    @Override
    public void onLoaderReset(Loader<ContentWrapper<ApplyOrderModel>> loader) {
        mGridAdapter.setModels(new DummyWrapper());
    }

}
