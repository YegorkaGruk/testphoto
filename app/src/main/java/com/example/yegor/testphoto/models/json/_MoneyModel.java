package com.example.yegor.testphoto.models.json;

public class _MoneyModel {

    public float amount;
    public String currency;
    public String symbol;

    public _MoneyModel() {
    }

    public _MoneyModel(float amount, String currency, String symbol) {
        this.amount = amount;
        this.currency = currency;
        this.symbol = symbol;
    }

    public String getMessage() {
        return symbol + " " + amount;
    }

    @Override
    public String toString() {
        return "_MoneyModel{" +
                "amount=" + amount +
                ", currency='" + currency + '\'' +
                ", symbol='" + symbol + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        _MoneyModel that = (_MoneyModel) o;

        if (Float.compare(that.amount, amount) != 0) return false;
        if (currency != null ? !currency.equals(that.currency) : that.currency != null)
            return false;
        return symbol != null ? symbol.equals(that.symbol) : that.symbol == null;

    }


    @Override
    public int hashCode() {
        int result = (amount != +0.0f ? Float.floatToIntBits(amount) : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (symbol != null ? symbol.hashCode() : 0);
        return result;
    }
}
